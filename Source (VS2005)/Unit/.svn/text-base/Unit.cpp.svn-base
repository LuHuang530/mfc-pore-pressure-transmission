//Unit.cpp 

//Created by Dr. Mengjiao Yu
//Modified on Feb. 10, 2007

#include "stdafx.h"

#include "Unit.h"
#include "UnitsTable.h"

//------------------------Implementation of Unit---------------------------------------
Unit::Unit()
{
	//units.clear(); //remove all, don't need this:)
}
Unit::Unit(const Unit& u)
{
	//units.clear();//remove all, don't need this:)
	name=u.name;
	SI=u.SI;
	units=u.units;//copy the vector...
}
Unit::Unit(const string& ns, const string& si)
{
	name=ns;
	SI=si;
};
Unit::~Unit(void)
{
	units.clear();//remove all
}
Unit& Unit::operator =(const Unit& u)
{
	if (this != &u)
	  {
		units.clear();//remove all
		name=u.name;
		SI=u.SI;
		units=u.units;
	  }
	return *this;
}
double Unit::ToSI(double value, const string& unit_name_CString, int& ErrStatus)
{
	ErrStatus=0;
	for (vector<Unit_Convertor>::iterator i=units.begin();i!=units.end();i++)
		{
			if (unit_name_CString==(*i).Unit_Name) return ( ((*i).k )* value + ((*i). b) );
		}
	ErrStatus=Unit_Not_Found;
	return 0; 
	//throw Unit_Not_Found;
}
double Unit::SIToUnit(double value, const string& unit_name_CString, int& ErrStatus)
{
	ErrStatus=0;
	for (vector<Unit_Convertor>::iterator i=units.begin();i!=units.end();i++)
		{
			if (unit_name_CString==(*i).Unit_Name) return ( value -(*i).b ) /((*i).k );
		}
	ErrStatus=Unit_Not_Found;
	return 0; 
	//return 0;//should throw exception....
}
void Unit::InsertUnitConvertor(const Unit_Convertor& c)
{
	//add it only when it is not in the list
	if (! IsUnit(c.Unit_Name))
		units.push_back(c); //add it into the unit store...
}
void Unit::Merge(const Unit& u)
{
	if(u.name == name)		//only when they have the same unit type name.
		for (vector<Unit_Convertor>::const_iterator i=u.units.begin();i!=u.units.end();i++)
			InsertUnitConvertor(*i);//add to the unit
		
}
double Unit::Convert(double value, const string& fromUnit, const string& toUnit, int& ErrStatus)
{
	double result=ToSI(value,fromUnit,ErrStatus);
	if (!ErrStatus)
		result=SIToUnit(result, toUnit, ErrStatus);//that's it. Unit->SI->NewUnit..
	return result;
	/*	//didn't handle exceptions--- not a valid unit name.
	double result=0.0;
	try
	{
		result=SIToUnit(ToSI(value,fromUnit), toUnit);//that's it. Unit->SI->NewUnit..
	}
	catch(...)
	{
		throw Unit_Not_Found;
	}
	return result;
	*/
}

const char* Unit::UnitName(unsigned int i)
{
	if (i>=Size() || i<0) 
		//return NULL;
		return ""; //Modified by Dr. Mengjiao Yu, Date: 02/07/08
	else
		return units[i].Unit_Name.c_str();
}
unsigned int Unit::Size()
{
	return (unsigned int)units.size();//return the size of the vector.
}
unsigned int Unit::MaxUnitNameLength()
{
	unsigned int len=0;
	for (vector<Unit_Convertor>::const_iterator i=units.begin();i!=units.end();i++)
		if ( (*i).Unit_Name.size()>len )
			len=(unsigned int)(*i).Unit_Name.size();
	return len;
}
ostream& operator << (ostream& outs, const Unit& u)		//not tested yet.
 {
	outs<<"\""<<  BEGIN_TOKEN  <<      "\"\t"<<"\""<<u.name<<"\"\t"<<"\""<<u.SI<<"\""<<endl;//output leading symbol & name, SI
	for(vector<Unit_Convertor>::const_iterator j =u.units.begin(); j!=u.units.end();j++)
	{
		//for each every unit convertor, output the name, k and b;
		outs<<"\""<<(*j).Unit_Name <<"\"\t";	//output the name
		outs<<(*j).k<<"\t"<<(*j).b<<endl;		//output k and b
	}
	outs<<"\""<<  END_TOKEN<< "\"";//out ending symbol
	return outs;
 };
istream& operator >>(istream& ins, Unit& u)
 {
	//Handling the units here. 
	string tmpS;

	BypassComment(ins);//skip white spaces...
	tmpS=ReadAString(ins); //use this instead of >>
	if (tmpS!=BEGIN_TOKEN) 
		{
			return ins; //sorry, cann't handle this because of wrong file...
		}
	
	BypassComment(ins);
	u.name=ReadAString(ins); //use this instead of >>

	if (u.name==END_TOKEN)
		{
			return ins;//end up with an empty unit
		}
	
	//now, real one.
	BypassComment(ins);
	u.SI=ReadAString(ins);//read in the SI unit (name CString)

	Unit_Convertor a;
	//Modified  by Mengjiao Yu on July 2, 2007 -----------------
	//to take care of missing SI Unit in the list
	//for a SI unit, k=1 and b=0 by default.
	a.Unit_Name=u.SI;
	a.k=1.0;
	a.b=0.0;
	u.InsertUnitConvertor(a);//add the SI unit convertor into the list

	//----------------------------------------------------------
	do
	{
		BypassComment(ins);//skip white spaces
		a.Unit_Name =ReadAString(ins); //read in name...
		
		if (a.Unit_Name == END_TOKEN)
			{
				return ins;//end up with an empty unit
			}
		BypassComment(ins);//
		ins>>a.k;    //get k
		BypassComment(ins);
		ins>>a.b;    //get b
		//modified by Mengjiao Yu on July 2, 2007--------------------------------------------
		//replaced the following statement with
		//u.units.push_back(a);//add a convertor in the unit.....
		u.InsertUnitConvertor(a);//this will ensure the check of duplicated unit convertor
		//-----------------------------------------------------------------------------------

		BypassComment(ins);
	}while (a.Unit_Name!=END_TOKEN || (! (ins.eof()) ) );
	return ins;
  };

const vector<Unit_Convertor>* Unit::GetUnitsList()
{
	return &units;
}
bool Unit::IsEmpty() const
{
	return (units.size()<=0);
}
const string& Unit::GetName() const
{
	return name;
}
const string& Unit::GetSI() const
{
	return SI;
}
bool Unit::IsUnit(const string& Uname)				//not tested yet.
{
	for(vector<Unit_Convertor>::const_iterator j =units.begin(); j!=units.end();j++)
			if( (*j).Unit_Name==Uname) return true;//found the unit, return true
	return false;
}

//-------------------------Implementation of UnitCollection -----------------------------------------
UnitCollection* UnitCollection::_instance=0;	//for singleton purpose. 
list<Unit*> UnitCollection::container;
string UnitCollection::UnitFileName="";			//
vector<string> UnitCollection::Languages;
vector<Translator> UnitCollection::TranslatorContainer;
string UnitCollection::CurrentLanguage="English"; //default language is English
UnitCollection::UnitCollection()
{
	//we have a table of unit converters
	//it is stored in UnitsTable.h, [Note the string has to be in one line!]
	//it is defined in a string
	//we convert it to istringstream [Need #include <sstream>]
	//then follow the same io using general.cpp 
	istringstream Units0(UnitsTable);
	BypassComment(Units0);
	while (! Units0.eof())
		{
			Unit a;
			Units0>>a; //read in one...
			AddUnit(a);//add it into the collection
			BypassComment(Units0); //skip white spaces...
			////////
			Translator t;			//alse create a English to English Translator.
			//Firstly, Add the Unit Type name into the Translator Container
			t.English=a.GetName();
			t.LanguageName="English";
			t.Translated=a.GetName();
			if(! IsLanguage(t.LanguageName))  
					UnitCollection::Languages.push_back(t.LanguageName);//add language into the list
			if (! IsTranslator(t) )
					UnitCollection::TranslatorContainer.push_back(t);//add it in
			//Then add all the unit convertor names into the translator container.

			if (t.English!="") //there is a unit type name..
			{
				if(! IsLanguage(t.LanguageName)) 
					UnitCollection::Languages.push_back(t.LanguageName);//add language into the list
				//one by one
				const vector<Unit_Convertor>* p=a.GetUnitsList();
				if (p)	//add all convertors 
					for (vector<Unit_Convertor>::const_iterator i=p->begin();i!=p->end();i++)
					{
						t.English=(*i).Unit_Name;
						t.Translated=(*i).Unit_Name ;
						t.LanguageName="English";
						if (! IsTranslator(t) )
							UnitCollection::TranslatorContainer.push_back(t);//add it in
					}
			}

		}
		if (! IsLanguage("English"))
			UnitCollection::Languages.push_back("English");//set default Language.
}
UnitCollection::~UnitCollection()
{
	while (container.size()>0)
	{
		Unit* tmp=container.front();
		container.pop_front();//pop front,
		delete tmp;
	}
	UnitCollection::Languages.clear();
	UnitCollection::TranslatorContainer.clear();
}
Unit* UnitCollection::GetUnit(const string& UnitName)
{
	if (UnitName.size()<=0) return NULL; //zero length string, 
	for (list<Unit*>::iterator i=container.begin();i!=container.end();i++)
	{
		if ( UnitName == (*i)->GetName() || (*i)->IsUnit(UnitName) ) 
						return (*i);//find it...Unit Type Name or Unit Name
	}
	return NULL;
}
bool UnitCollection::Create(const string& filename)
{
	//open filename and create the units.
	try
	{	
		ifstream ins(filename.c_str());
		//Turn on IO exceptions to handle file tail (white space after the last input) problem.
		ins.exceptions ( ifstream::eofbit | ifstream::failbit | ifstream::badbit ); 

		if (ins.fail()) return false; //check if something is wrong..
		BypassComment(ins);
		while (! ins.eof())
		{
			Unit a;
			ins>>a; //read in one...
			AddUnit(a);//add it into the collection			
			BypassComment(ins); //skip white spaces...
			////////
			Translator t;			//alse create a English to English Translator.
			//Firstly, Add the Unit Type Name into the translator container.
			t.English=a.GetName();
			t.LanguageName="English";
			t.Translated=a.GetName();
			if(! IsLanguage(t.LanguageName))  
					UnitCollection::Languages.push_back(t.LanguageName);//add language into the list
			if (! IsTranslator(t) )
					UnitCollection::TranslatorContainer.push_back(t);//add it in
			//Then add all the unit convertor names into the translator container.
			if (t.English!="") //there is a unit type name..
			{
				if(! IsLanguage(t.LanguageName)) 
					UnitCollection::Languages.push_back(t.LanguageName);//add language into the list
				//one by one
				const vector<Unit_Convertor>* p=a.GetUnitsList();
				if (p)	//add all convertors 
					for (vector<Unit_Convertor>::const_iterator i=p->begin();i!=p->end();i++)
					{
						t.English=(*i).Unit_Name;
						t.Translated=(*i).Unit_Name ;
						t.LanguageName="English";
						if (! IsTranslator(t) )
							UnitCollection::TranslatorContainer.push_back(t);//add it in
					}
			}

		}
		ins.close();//close the file...
	}catch(...)
	{
		cout<<"Creating(): Something is wrong "<<endl;
		return false;//
	}
	return true;
}
bool UnitCollection::Dump(const string& filename)
{
	//dump the factors to a file.
	try
	{	
		ofstream outs(filename.c_str());
		//Turn on IO exceptions to handle file tail (white space after the last input) problem.
		outs.exceptions ( ofstream::eofbit | ofstream::failbit | ofstream::badbit ); 
		if (outs.fail()) return false; //check if something is wrong..
		outs<<*UnitCollection::Instance();	//use << operator to do it.
		outs.close();//close the file...
	}catch(...)
	{
		cout<<"Dumping(): Something is wrong "<<endl;
		return false;	//Hmmm... Should throw exceptions
	}
	return true;
}

UnitCollection* UnitCollection::Instance(const string& uf)
{
	if (_instance==0) _instance=new UnitCollection;
	if (uf!="" && uf!=UnitFileName)
	{
		Create(uf);//create the Unit conversion system based on the file.
		UnitFileName=uf;//keep a copy of the unit file name
	}	
	return _instance;
}
const list<Unit*>* UnitCollection::GetUnitTypeList()
{
	return &container;
}
bool UnitCollection::IsUnitType(const string& Uname)		//not tested yet.
{
	for (list<Unit*>::const_iterator i=container.begin();i!=container.end();i++)
		if( (*i)->GetName()==Uname) return true;	//found the name, return true
	return false;
}
void UnitCollection::AddUnit(const Unit& u)
{
	if  ((u.IsEmpty()) )  return; //if it is an empty unit, just bypass it....
	//first check if the unit to be added is already in the collection
	Unit* p=UnitCollection::GetUnit(u.GetName());
	if (p)
		//unit to be added is already in the collection
		//add unit items to the unit (maybe new unit items are available)
		p->Merge(u);
	else
		{	//unit to be added is not in the collection
			Unit* ptrUnit=new Unit(u);	//make a copy of the unit to be added
			container.push_back(ptrUnit);//add it to the collection...
		}
}
unsigned int UnitCollection::Size()
{
	return (int)container.size();
}
const char* UnitCollection::UnitTypeName(unsigned int index)
{
	if (index <0 || index >= Size())
		//return NULL;
		return ""; //Modified by Dr. Mengjiao Yu Date: 02/07/08
	else
	{
		list<Unit*>::iterator i=container.begin();
		while (index--) i++;					//kind of stupid
		return (*i)->GetName().c_str();			//find the name
	}
}
unsigned int UnitCollection::MaxUnitNameLength()
{
	unsigned int len=0;
	for (list<Unit*>::const_iterator i=container.begin();i!=container.end();i++)
	{
		//check the unit type name first
		if ((*i)->GetName().size()>len) 
			len=(unsigned int)(*i)->GetName().size();
		//then check all the units in this type.
		if( (*i)->MaxUnitNameLength() > len) 
			len=(*i)->MaxUnitNameLength();
	}
	return len;
}
bool UnitCollection::IsLanguage(const string& Lang)
{
	for (vector<string>::const_iterator i=UnitCollection::Languages.begin(); i!=UnitCollection::Languages.end();i++)
		if ((*i)==Lang) return true;		//search one by one, found it 
	return false;
}
bool UnitCollection::IsTranslator(const Translator& x)
{
	for (vector<Translator>::const_iterator i=UnitCollection::TranslatorContainer.begin();i!=UnitCollection::TranslatorContainer.end();i++)
		if ((*i).English==x.English && (*i).LanguageName==x.LanguageName && (*i).Translated==x.Translated)
			return true;
	return false;
}
const string UnitCollection::TranslateToLanguage(const string& English, const string& Lang)
{
	for (vector<Translator>::const_iterator i=UnitCollection::TranslatorContainer.begin(); i!=UnitCollection::TranslatorContainer.end();i++)
		if (((*i).English ==English) && ((*i).LanguageName==Lang)) return (*i).Translated ;		//search one by one, found it
	return "";			//not found... return an empty string
}
const string UnitCollection::TranslateToEnglish(const string& Translated)
{
	for (vector<Translator>::const_iterator i=UnitCollection::TranslatorContainer.begin(); i!=UnitCollection::TranslatorContainer.end();i++)
		if ((*i).Translated==Translated) return (*i).English  ;		//search one by one, found it
	return "";			//not found... return an empty string
}
bool UnitCollection::DumpTranslator(const string& filename)
{
	//dump the factors to a file.
	try
	{	
		ofstream outs(filename.c_str());
		//Turn on IO exceptions to handle file tail (white space after the last input) problem.
		outs.exceptions ( ofstream::eofbit | ofstream::failbit | ofstream::badbit ); 
		if (outs.fail()) return false; //check if something is wrong..
		//first write UTF8 BOM into the file (for easy handling by other program
		unsigned char x=0xef; unsigned char y=0xbb; unsigned char z=0xbf;
		outs<<x<<y<<z;
		for (vector<Translator>::const_iterator i=UnitCollection::TranslatorContainer.begin(); i!=UnitCollection::TranslatorContainer.end();i++)
			outs<<"\""<<(*i).English<<"\"\t    "<<"\""<<(*i).LanguageName<<"\"\t    "<<"\""<<(*i).Translated<<"\""<<endl;
	}catch(...)
	{
		cout<<"Dumping Translator(): Something is wrong "<<endl;
		return false;	//Hmmm... Should throw exceptions
	}
	return true;
}
bool UnitCollection::LoadTranslator(const string& filename)
{
	//open filename and create the units.
	try
	{	
		ifstream ins(filename.c_str());
		//Turn on IO exceptions to handle file tail (white space after the last input) problem.
		ins.exceptions ( ifstream::eofbit | ifstream::failbit | ifstream::badbit ); 

		if (ins.fail()) return false; //check if something is wrong..
		//handle BOM: EF BB BF 3 char
		unsigned char x=0,y=0,z=0;
		if (!ins.eof())ins>>x;
		if (!ins.eof())ins>>y;
		if (!ins.eof())ins>>z;
		if (ins.eof()) return false;//shouldn't be empty
		//if x y z are not UTF8 BOM, put it back to strem
		if (!((x==0xef) && (y==0xbb) && (z==0xbf)))
		{
			ins.putback(z);
			ins.putback(y);
			ins.putback(x);
		}
		//read in the translator
		BypassComment(ins);
		while (! ins.eof())
		{
			Translator t;
			t.English=ReadAString(ins);
			BypassComment(ins);
			t.LanguageName=ReadAString(ins);
			BypassComment(ins);
			t.Translated=ReadAString(ins) ;	//read in a Translator.

			if (! IsLanguage(t.LanguageName)) 
				UnitCollection::Languages.push_back(t.LanguageName);//add the language name into the list 
			if (! IsTranslator(t))
				UnitCollection::TranslatorContainer.push_back(t);		//add it into the container
																	//should check redundancy
			BypassComment(ins); //skip white spaces...
		}
		ins.close();//close the file...
	}catch(...)
	{
		cout<<"Loading Translator(): Something is wrong "<<endl;
		return false;//
	}
	return true;
}
const string UnitCollection::Translate(const string& EnglishL, const string& LanguageName)
{
	if (LanguageName=="English") return EnglishL;//English to English, don't do anything.
	//translate from english to another language specified by LanguageName
	for (vector<Translator>::const_iterator i=UnitCollection::TranslatorContainer.begin(); i!=UnitCollection::TranslatorContainer.end();i++)
			if( ((*i).LanguageName==LanguageName) && ((*i).English==EnglishL))
				return (*i).Translated;
	return "";			//couldn't find it. return an empty string
}
const string UnitCollection::Translate(const string& OtherLanguage)
{
	for (vector<Translator>::const_iterator i=UnitCollection::TranslatorContainer.begin(); i!=UnitCollection::TranslatorContainer.end();i++)
		if ((*i).Translated == OtherLanguage) return (*i).English;
	return "";					//couldn't find it. Return an empty string
}


int  UnitCollection::UnitCategoriesCount()
{
	UnitCollection* ptr=UnitCollection::Instance("Units.txt"); //this creates the unit converion system...
	if (ptr)
	{
		const list<Unit*>* tmp=ptr->GetUnitTypeList();//get  the list....
		if (tmp) return (int)tmp->size();
		else
			return -1;
	}
	else
		return -1;

}
int  UnitCollection::UnitCount()
{
	UnitCollection* ptr=UnitCollection::Instance("Units.txt"); //this creates the unit converion system...
	if (!ptr) return -1;	//error
	const list<Unit*>* tmp=ptr->GetUnitTypeList();//get  the list....
	if (!tmp) return -1;	//error
	int totalUnits=0;
	for (list<Unit*>::const_iterator i=tmp->begin();i!=tmp->end();i++)
	{
		
		Unit* pUnit=*i;
		//for each every unit type, display the units that can be converted
		const vector<Unit_Convertor>* tmpU=pUnit->GetUnitsList(); //get the pointer for the list of Unit
		if (!tmpU) return -1;
		totalUnits += (int)tmpU->size();
	};
	return totalUnits;
}


ostream& operator << (ostream& outs, const UnitCollection& u)
{
	try
	{
		for (list<Unit* >::const_iterator i=u.container.begin();i!=u.container.end();i++)
			outs<< *(*i);
	}
	catch(...)
	{
		throw ;//re-throw io stream failure.
	}
	return outs;
}
istream& operator >> (istream& ins, UnitCollection& u)
{
	try
	{
		BypassComment(ins);	
		while(! ins.eof())
		{
			Unit a;
			ins>>a;
			u.AddUnit(a);
			BypassComment(ins);
		}	
	}
	catch(...)
	{
		throw ;//re-throw io stream failure.
	}
	return ins;
}