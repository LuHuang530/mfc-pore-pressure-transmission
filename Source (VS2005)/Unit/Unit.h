//Unit convertion version 0.1
//Unit.h
//Dr. Mengjiao Yu
//Modified on Feb. 10, 2007
//
//Professionals working in engineering domains often suffer from the painful
//"Unit conversion" problem. This code will give the users an easy way to 
//handle the problem.
//This code can be used freely for educational/academic purpose
//Unit conversion: kx+b=SI

/*___________________________________________________________________________________
//How to use the unit conversion system:
//Use the following sequence to do the unit conversion.....
		
		1.Initiallization
				using "Units.txt" file to create the unit conversion system first
				this is required only for the first call of Instance() function.
		   UnitCollection* ptr=UnitCollection::Instance("Units.txt"); 
		2.Convert units..
		   Unit* pU=ptr->GetUnit("Length"); //get the unit type 
		   (or Unit* pU=ptr->GetUnit("cm"); //a specific unit also works)
		   double x=pU->ToSI(8,"in"); //convert 8 inches to SI unit
		   double y=pU->Convert(8, "in", "ft");//convert 8 inches to ft
Note:
		1. file name "Units.txt" is needed only for the first time when you call Instance() function.
		2.after creating the unit conversion system, 
			you can use the following sequence to convert the units..
		UnitCollection* ptr=UnitCollection::Instance(); //get the unit conversion system
		Unit* pU=ptr->GetUnit("Length"); //get the unit type 
		double x=pU->ToSI(8,"in"); //convert 8 inches to SI unit
		double y=pU->Convert(8, "in", "ft");//convert 8 inches to ft
___________________________________________________________________________________________*/

#ifndef UNIT_H
#define UNIT_H

#define Unit_Not_Found	101						//for exception
#define BEGIN_TOKEN		"UNIT_BEGIN"			//leading string for a unit group
#define END_TOKEN		"UNIT_END"				//terminating string for a unit group, must be in pairs

#include <assert.h>
#include <fstream>
#include <vector>
#include <string>
#include <list>

#include "../general.h"

using namespace std;


//------------------------define Unit Convertor ----------------------------------------- 
class Unit_Convertor 
{
	public:
		string Unit_Name;
		double k;
		double b;
};
//--------------------------define Translator-------------------------------------------
class Translator
{
	public:
		string English;					//English words
		string LanguageName;			//target language
		string Translated;				//translated words to target language
};

//------------------------------define Unit----------------------------------------------
class Unit
{
	public:
		Unit(void);
		Unit(const Unit& u);//copy constructor....
		Unit(const string& ns, const string& si);
		~Unit(void);
	public:
		Unit& operator = (const Unit& u);
	public:
		const string& GetSI() const;				//get SI unit
		bool IsEmpty() const;					//test if it is empty
		const string& GetName() const;
		const vector< Unit_Convertor >* GetUnitsList();		//get a list of unit converters.
		bool IsUnit(const string& Uname);			//check if Uname is an Unit

		void InsertUnitConvertor(const Unit_Convertor& c);
		void Merge(const Unit& u);				//copy the converters to this unit

		double ToSI(double value, const string& unit_name_CString, int& ErrStatus);
		double SIToUnit(double value, const string& unit_name_CString, int& ErrStatus);
		double Convert(double value, const string& fromUnit, const string& toUnit, int& ErrStatus);	//convert

		//char** UnitList();		//return a list of possible units
		const char* UnitName(unsigned int i);// return the ith unit name. NULL if out of range
		unsigned int Size(); //return the number of Units in this group
		unsigned int MaxUnitNameLength();						//return the maximum string length of the units

protected:
		string name;				//the name of this unit
		string SI;					//SI unit (string)
		vector<Unit_Convertor> units;
		
	//friend for IO
		friend ostream& operator << (ostream& outs, const Unit& u);
		friend istream& operator >>(istream& ins, Unit& u);
};

// ----------------------- Define the collection of units -----------------------------------
//Singleton is adopted to ensure only one copy will be created.....
class UnitCollection 
{
	protected:
		UnitCollection();
		~UnitCollection();
	public:
		static bool Create(const string& filename);			//create from a file...
		static bool Dump(const string& filename);			//dump to a file.
		static Unit* GetUnit(const string& UnitName);
		static UnitCollection* Instance(const string& uf="");		//initialization or get the instance
		static const list<Unit*>* GetUnitTypeList();				// get a list of unit types 
		static bool IsUnitType(const string& Uname);				//check if Uname is an Unit Type
		static void AddUnit(const Unit& u);						//add a unit to the collection
		static unsigned int Size();
		static const char* UnitTypeName(unsigned int index);		
		static unsigned int MaxUnitNameLength();						//return the maximum string length of the units
		static bool IsLanguage(const string& Lang);				//check if Lang is a language
		static bool IsTranslator(const Translator& x);			//check if x is a Translator
		static const string TranslateToLanguage(const string& English, const string& Lang); //translate english to targeted language
		static const string TranslateToEnglish(const string& Translated); //translate "Translated" to English
		static bool DumpTranslator(const string& filename);		//dump translator to a file.
		static bool LoadTranslator(const string& filename);		//load translator from a file
		static int  UnitCategoriesCount();						//total number of unit categories
		static int  UnitCount();								//total number of total units

	protected:
		static list<Unit*> container;						//only one copy....
		static UnitCollection* _instance;					//for singleton purpose...
		static string UnitFileName;							//string of the Unit file Name
	public:	
		static string CurrentLanguage;						//language selected for use
		static vector<string> Languages;					//list of languages supported
		static vector<Translator> TranslatorContainer;		//holds all translations.
		static const string Translate(const string& EnglishL, const string& LanguageName); //translate from english to target Language
		static const string Translate(const string& OtherLanguage);//translate to English
	//friend, I/O
		friend ostream& operator << (ostream& outs, const UnitCollection& u);
		friend istream& operator >> (istream& ins, UnitCollection& u);
};

#endif
