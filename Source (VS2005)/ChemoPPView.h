// ChemoPPView.h : interface of the CChemoPPView class
//


#pragma once


class CChemoPPView : public CView
{
protected: // create from serialization only
	CChemoPPView();
	DECLARE_DYNCREATE(CChemoPPView)

// Attributes
public:
	CChemoPPDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// Implementation
public:
	virtual ~CChemoPPView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in ChemoPPView.cpp
inline CChemoPPDoc* CChemoPPView::GetDocument() const
   { return reinterpret_cast<CChemoPPDoc*>(m_pDocument); }
#endif

