// ChemoPPDoc.h : interface of the CChemoPPDoc class
//


#pragma once


class CChemoPPDoc : public CDocument
{
protected: // create from serialization only
	CChemoPPDoc();
	DECLARE_DYNCREATE(CChemoPPDoc)

// Attributes
public:

// Operations
public:

// Overrides
public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

// Implementation
public:
	virtual ~CChemoPPDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()

public:
	BOOL OnSaveDocument(LPCTSTR lpszPathName);
	afx_msg void OnFileSaveAs();
	afx_msg void OnFileSave();
};


