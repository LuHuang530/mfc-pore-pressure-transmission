// LinearInput.cpp : implementation file
//

#include "stdafx.h"
#include "ChemoPP.h"
#include "LinearInput.h"
#include "Unit/Unit.h"
#include "Calc/LinearPP.h"

#include "MainFrm.h"
#include "ChildFrm.h"
#include "ChartView.h"


// CLinearInput dialog

IMPLEMENT_DYNAMIC(CLinearInput, CPropertyPage)

CLinearInput::CLinearInput()
	: CPropertyPage(CLinearInput::IDD)
{

}

CLinearInput::~CLinearInput()
{
}

void CLinearInput::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO1, cLinearLength);
	DDX_Control(pDX, IDC_COMBO11, cLinearC0);
	DDX_Control(pDX, IDC_COMBO3, cLinearP0);
	DDX_Control(pDX, IDC_COMBO4, cLinearCf);
	DDX_Control(pDX, IDC_COMBO2, cLinearTemp);
	DDX_Control(pDX, IDC_COMBO9, cLinearTime);
	DDX_Control(pDX, IDC_COMBO5, cLinearCdf);
	DDX_Control(pDX, IDC_COMBO6, cLinearPdf);
	DDX_Control(pDX, IDC_COMBO7, cLinearDt);
	DDX_Control(pDX, IDC_COMBO8, cLinearDx);
	
	DDX_Control(pDX, IDC_EDIT1, tLinearLength);
	DDX_Control(pDX, IDC_EDIT2, tLinearC0);
	DDX_Control(pDX, IDC_EDIT3, tLinearP0);
	DDX_Control(pDX, IDC_EDIT4, tLinearCf);
	DDX_Control(pDX, IDC_EDIT7, tLinearNions);
	DDX_Control(pDX, IDC_EDIT8, tLinearMw);
	DDX_Control(pDX, IDC_EDIT12, tLinearTemp);
	DDX_Control(pDX, IDC_EDIT13, tLinearTime);
	DDX_Control(pDX, IDC_EDIT5, tLinearCdf);
	DDX_Control(pDX, IDC_EDIT6, tLinearPdf);
	DDX_Control(pDX, IDC_EDIT14, tLinearDt);
	DDX_Control(pDX, IDC_EDIT15, tLinearDx);
	DDX_Control(pDX, IDC_EDIT9, tLinearK1);
	DDX_Control(pDX, IDC_EDIT10, tLinearK2);
	DDX_Control(pDX, IDC_EDIT11, tLinearDeff);
}

BOOL CLinearInput::OnInitDialog()
{
	CPropertyPage::OnInitDialog();
	ComboBoxFill();

	return TRUE;
}

void CLinearInput::Compute(vector<double>& Pdata, vector<double>& tdata)
{
	CLinearInput::Read();
	h.Compute(h, Pdata, tdata);
}

void CLinearInput::ComboBoxFill()
{
	UnitCollection* ptr = UnitCollection::Instance("Units.txt"); //initialize
	
	//populating comboboxes
	Unit* pU;
	const vector<Unit_Convertor>* pUl;
	vector<Unit_Convertor>::const_iterator i;
	std::string s;
	CString cs;

	//length unit type
	pU = ptr->GetUnit("Length");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		s = (*i).Unit_Name;
		cs = s.c_str();
		
		cLinearLength.AddString(cs); //core sample length
		cLinearDx.AddString(cs); //dx
	}
	
	//pressure unit type
	pU = ptr->GetUnit("Pressure");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		s = (*i).Unit_Name;
		cs = s.c_str();

		cLinearP0.AddString(cs); //initial pore pressure
		cLinearPdf.AddString(cs); //test fluid pressure
	}

	//concentration (mass basis) unit type
	pU = ptr->GetUnit("Concentration");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		s = (*i).Unit_Name;
		cs = s.c_str();

		cLinearCdf.AddString(cs); //test fluid concentration
	}

	//concentration (mol basis) unit type
	pU = ptr->GetUnit("Concentration (mol basis)");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		s = (*i).Unit_Name;
		cs = s.c_str();

		cLinearC0.AddString(cs); //initial pore fluid concentration
	}

	//compressibility unit type
	pU = ptr->GetUnit("Compressibility");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		s = (*i).Unit_Name;
		cs = s.c_str();

		cLinearCf.AddString(cs); //pore fluid compressibility
	}

	//temperature unit type
	pU = ptr->GetUnit("Temperature");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		s = (*i).Unit_Name;
		cs = s.c_str();
		
		cLinearTemp.AddString(cs); //temperature
	}

	//time unit type
	pU = ptr->GetUnit("Time");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		s = (*i).Unit_Name;
		cs = s.c_str();
		
		cLinearTime.AddString(cs); //total time
		cLinearDt.AddString(cs); //dt
	}
}

BOOL CLinearInput::Open(CString lpszPathName)
{
	//read from file
	ifstream ins(lpszPathName);
	ins >> h;
	ins.close();

	CString cs;

	cs.Format(_T("%.3f"), h.L);
	tLinearLength.SetWindowTextW(cs);
	cs = (h.uL).c_str();
	cLinearLength.SelectString(-1,cs);

	cs.Format(_T("%.2f"), h.C0);
	tLinearC0.SetWindowTextW(cs);
	cs = (h.uC0).c_str();
	cLinearC0.SelectString(-1,cs);

	cs.Format(_T("%.2f"), h.P0);
	tLinearP0.SetWindowTextW(cs);
	cs = (h.uP0).c_str();
	cLinearP0.SelectString(-1,cs);

	cs.Format(_T("%.2e"), h.cf);
	tLinearCf.SetWindowTextW(cs);
	cs = (h.ucf).c_str();
	cLinearCf.SelectString(-1,cs);

	cs.Format(_T("%i"), h.n);
	tLinearNions.SetWindowTextW(cs);

	cs.Format(_T("%.2f"), h.M);
	tLinearMw.SetWindowTextW(cs);

	cs.Format(_T("%.2f"), h.Temp);
	tLinearTemp.SetWindowTextW(cs);	
	cs = (h.uTemp).c_str();
	cLinearTemp.SelectString(-1,cs);

	cs.Format(_T("%.1f"), h.t);
	tLinearTime.SetWindowTextW(cs);	
	cs = (h.ut).c_str();
	cLinearTime.SelectString(-1,cs);

	cs.Format(_T("%.2f"), h.Cdf);
	tLinearCdf.SetWindowTextW(cs);
	cs = (h.uCdf).c_str();
	cLinearCdf.SelectString(-1,cs);

	cs.Format(_T("%.2f"), h.Pdf);
	tLinearPdf.SetWindowTextW(cs);
	cs = (h.uPdf).c_str();
	cLinearPdf.SelectString(-1,cs);

	cs.Format(_T("%.1f"), h.dt);
	tLinearDt.SetWindowTextW(cs);
	cs = (h.udt).c_str();
	cLinearDt.SelectString(-1,cs);

	cs.Format(_T("%.3f"), h.dx);
	tLinearDx.SetWindowTextW(cs);
	cs = (h.udx).c_str();
	cLinearDx.SelectString(-1,cs);

	cs.Format(_T("%.2e"), h.K1);
	tLinearK1.SetWindowTextW(cs);

	cs.Format(_T("%.2e"), h.K2);
	tLinearK2.SetWindowTextW(cs);

	cs.Format(_T("%.2e"), h.Deff);
	tLinearDeff.SetWindowTextW(cs);
	
	return TRUE;
}

BOOL CLinearInput::Save(CString lpszPathName)
{
	CLinearInput::Read();

	//save to file
	ofstream outs(lpszPathName);
	outs << h;
	outs.close();
	return TRUE;
}

void CLinearInput::Read()
{
	CString cs;

	tLinearLength.GetWindowTextW(cs);
	h.L = atof(CT2CA(cs));
	cLinearLength.GetWindowTextW(cs);
	h.uL = CT2CA(cs);

	tLinearC0.GetWindowTextW(cs);
	h.C0 = atof(CT2CA(cs));
	cLinearC0.GetWindowTextW(cs);
	h.uC0 = CT2CA(cs);

	tLinearP0.GetWindowTextW(cs);
	h.P0 = atof(CT2CA(cs));
	cLinearP0.GetWindowTextW(cs);
	h.uP0 = CT2CA(cs);

	tLinearCf.GetWindowTextW(cs);
	h.cf = atof(CT2CA(cs));
	cLinearCf.GetWindowTextW(cs);
	h.ucf = CT2CA(cs);

	tLinearNions.GetWindowTextW(cs);
	h.n = atoi(CT2CA(cs));

	tLinearMw.GetWindowTextW(cs);
	h.M = atof(CT2CA(cs));

	tLinearTemp.GetWindowTextW(cs);
	h.Temp = atof(CT2CA(cs));
	cLinearTemp.GetWindowTextW(cs);
	h.uTemp = CT2CA(cs);

	tLinearTime.GetWindowTextW(cs);
	h.t = atof(CT2CA(cs));
	cLinearTime.GetWindowTextW(cs);
	h.ut = CT2CA(cs);

	tLinearCdf.GetWindowTextW(cs);
	h.Cdf = atof(CT2CA(cs));
	cLinearCdf.GetWindowTextW(cs);
	h.uCdf = CT2CA(cs);

	tLinearPdf.GetWindowTextW(cs);
	h.Pdf = atof(CT2CA(cs));
	cLinearPdf.GetWindowTextW(cs);
	h.uPdf = CT2CA(cs);

	tLinearDt.GetWindowTextW(cs);
	h.dt = atof(CT2CA(cs));
	cLinearDt.GetWindowTextW(cs);
	h.udt = CT2CA(cs);

	tLinearDx.GetWindowTextW(cs);
	h.dx = atof(CT2CA(cs));
	cLinearDx.GetWindowTextW(cs);
	h.udx = CT2CA(cs);

	tLinearK1.GetWindowTextW(cs);
	h.K1 = atof(CT2CA(cs));

	tLinearK2.GetWindowTextW(cs);
	h.K2 = atof(CT2CA(cs));

	tLinearDeff.GetWindowTextW(cs);
	h.Deff = atof(CT2CA(cs));

	//get max number of plot points
	//find the chart view
	CChildFrame * pChild = (CChildFrame*)(((CMainFrame*)this->GetParentOwner())->MDIGetActive());
	CChartView* pChart = (CChartView*)pChild->m_wndSplitter.GetPane(0,1);
	pChart->cNPlot.GetWindowTextW(cs);
	h.nPlot = atoi(CT2CA(cs));
}

BEGIN_MESSAGE_MAP(CLinearInput, CPropertyPage)
END_MESSAGE_MAP()


// CLinearInput message handlers
