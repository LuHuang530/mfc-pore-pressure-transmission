// ChartView.cpp : implementation file
//

#include "stdafx.h"
#include "ChemoPP.h"
#include "ChartView.h"

#include "Chart/ChartSerie.h"
#include "Chart/ChartPointsSerie.h"
#include "Chart/ChartAxis.h"
#include "Chart/ChartAxisLabel.h"
#include "Chart/ChartString.h"

#include <vector>

using namespace std;


// CChartView

IMPLEMENT_DYNCREATE(CChartView, CFormView)

CChartView::CChartView()
	: CFormView(CChartView::IDD)
{

}

CChartView::~CChartView()
{
}

void CChartView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);

	DDX_Control(pDX, IDC_CHARTCTRL, m_ChartCtrl);
	DDX_Control(pDX, IDC_EDIT1, cNPlot);
}

void CChartView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	cNPlot.SetWindowTextW(_T("100"));
}

void CChartView::Plot(const vector<double>& Pdata, const vector<double>& tdata,const vector<double>& Edata,const vector<double>& ETdata)
{
	//assigning values to arrays (to use with chart control)
	double* PValues = NULL;
	double* tValues = NULL;

	double* EValues = NULL;
	double* ETValues = NULL;

	try
	{
		PValues = new double [Pdata.size()];
		tValues = new double [tdata.size()];
	}
	catch (bad_alloc& e)
	{
		if (PValues) delete[] PValues; PValues = NULL;
		if (tValues) delete[] tValues; tValues = NULL;
		throw e;
	}
	try
	{
		EValues = new double [Edata.size()];
		ETValues = new double [ETdata.size()];
	}
	catch (bad_alloc& e)
	{
		if (EValues) delete[] EValues; EValues = NULL;
		if (ETValues) delete[] ETValues; ETValues = NULL;
		throw e;
	}
	vector<double>::const_iterator i; int j;
	
	for (i=Pdata.begin(), j=0; i!=Pdata.end(); ++i, ++j)
		PValues[j] = *i;
	for (i=tdata.begin(), j=0; i!=tdata.end(); ++i, ++j)
		tValues[j] = *i;

	vector<double>::const_iterator x; int y;

	for (x=Edata.begin(), y=0; x!=Edata.end(); ++x, ++y)
		EValues[y] = *x;
	for (x=ETdata.begin(), y=0; x!=ETdata.end(); ++x, ++y)
		ETValues[y] = *x;
	
	//plotting
	m_ChartCtrl.EnableRefresh(false);

	m_ChartCtrl.RemoveAllSeries();
	
	CChartPointsSerie* pPointSeries = (CChartPointsSerie*)(m_ChartCtrl.AddSerie(CChartSerie::stPointsSerie));
	pPointSeries->SetPoints(tValues,PValues,(int)Pdata.size());
	pPointSeries->AddPoints(ETValues,EValues,(int)Edata.size());
	

	pPointSeries->SetPointType(CChartPointsSerie::ptEllipse);
	pPointSeries->SetPointSize(5,5);

	//set chart title
	TChartString chartTitle = _T("Pore Pressure Profile (Linear)");
	m_ChartCtrl.GetTitle()->RemoveAll();
	m_ChartCtrl.GetTitle()->AddString(chartTitle);

	//view legend
	m_ChartCtrl.GetLegend()->SetVisible(true);
	m_ChartCtrl.GetLegend()->DockLegend(CChartLegend::dsDockRight);
	
	//disable axis scroll bar
	CChartAxis* tAxis = m_ChartCtrl.GetBottomAxis();
	CChartAxis* PAxis = m_ChartCtrl.GetLeftAxis();

	tAxis->EnableScrollBar(false);
	PAxis->EnableScrollBar(false);
	
	//set axis range
	tAxis->SetAutomatic(true);
	PAxis->SetAutomatic(true);

	double tMin, tMax, PMin, PMax;
	tAxis->GetMinMax(tMin, tMax);
	PAxis->GetMinMax(PMin, PMax);

	tAxis->SetAutomatic(false);
	PAxis->SetAutomatic(false);

	tAxis->SetMinMax(tMin, 1.1*tMax);
	PAxis->SetMinMax(0 /*PMin*/, 1.1*PMax);

	//set axis label
	CChartAxisLabel* tAxisLabel = tAxis->GetLabel();
	CChartAxisLabel* PAxisLabel = PAxis->GetLabel();

	TChartString tLabel = _T("Time (sec)");
	TChartString PLabel = _T("Pore pressure (Pa)");

	tAxisLabel->SetText(tLabel);
	PAxisLabel->SetText(PLabel);

	m_ChartCtrl.EnableRefresh(true);
}

BEGIN_MESSAGE_MAP(CChartView, CFormView)
//	ON_WM_CREATE()
END_MESSAGE_MAP()


// CChartView diagnostics

#ifdef _DEBUG
void CChartView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CChartView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CChartView message handlers
