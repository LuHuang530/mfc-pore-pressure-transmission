#pragma once

#include "afxwin.h"
#include "Calc/LinearPP.h"


// CLinearInputPage dialog

class CLinearInputPage : public CPropertyPage
{
	DECLARE_DYNAMIC(CLinearInputPage)

public:
	CLinearInputPage();
	virtual ~CLinearInputPage();

// Dialog Data
	enum { IDD = IDD_LINEAR_INPUT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	LinearPP h;
	BOOL Open(LPCTSTR lpszPathName);
	BOOL Save(LPCTSTR lpszPathName);

public:
	CComboBox cLinearLength;
	CComboBox cLinearP0;
	CComboBox cLinearCf;
	CComboBox cLinearTemp;
	CComboBox cLinearTime;
	CComboBox cLinearCdf;
	CComboBox cLinearPdf;
	CComboBox cLinearDt;
	CComboBox cLinearDx;
	CComboBox cLinearC0;
	
	virtual BOOL OnInitDialog();
	void ComboBoxFill();
	
};
