#pragma once
#include "afxwin.h"


// CRadialInputPage dialog

class CRadialInputPage : public CPropertyPage
{
	DECLARE_DYNAMIC(CRadialInputPage)

public:
	CRadialInputPage();
	virtual ~CRadialInputPage();

// Dialog Data
	enum { IDD = IDD_RADIAL_INPUT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	CComboBox cRadialRw;
	CComboBox cRadialRe;
	CComboBox cRadialP0;
	CComboBox cRadialCf;
	CComboBox cRadialTemp;
	CComboBox cRadialTime;
	CComboBox cRadialCdf;
	CComboBox cRadialPw;
	CComboBox cRadialDt;
	CComboBox cRadialDr;
	CComboBox cRadialC0;

	virtual BOOL OnInitDialog();
	void ComboBoxFill();
};
