// TabView.cpp : implementation file
//

#include "stdafx.h"
#include "ChemoPP.h"
#include "TabView.h"


// CTabView

IMPLEMENT_DYNCREATE(CTabView, CFormView)

CTabView::CTabView()
	: CFormView(CTabView::IDD)
{

}

CTabView::~CTabView()
{
}

void CTabView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
}

void CTabView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	m_Tab  = new CTabSheet(_T("ChemoPP"),this);
	if (!m_Tab->Create(this, WS_CHILD | WS_VISIBLE, 0))
	{
		delete m_Tab;
		m_Tab = NULL;
	}
}

BEGIN_MESSAGE_MAP(CTabView, CFormView)
END_MESSAGE_MAP()


// CTabView diagnostics

#ifdef _DEBUG
void CTabView::AssertValid() const
{
	CFormView::AssertValid();
}

#ifndef _WIN32_WCE
void CTabView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif
#endif //_DEBUG


// CTabView message handlers
