// ChemoPPView.cpp : implementation of the CChemoPPView class
//

#include "stdafx.h"
#include "ChemoPP.h"

#include "ChemoPPDoc.h"
#include "ChemoPPView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CChemoPPView

IMPLEMENT_DYNCREATE(CChemoPPView, CView)

BEGIN_MESSAGE_MAP(CChemoPPView, CView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CView::OnFilePrintPreview)
END_MESSAGE_MAP()

// CChemoPPView construction/destruction

CChemoPPView::CChemoPPView()
{
	// TODO: add construction code here

}

CChemoPPView::~CChemoPPView()
{
}

BOOL CChemoPPView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CChemoPPView drawing

void CChemoPPView::OnDraw(CDC* /*pDC*/)
{
	CChemoPPDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
}


// CChemoPPView printing

BOOL CChemoPPView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CChemoPPView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CChemoPPView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}


// CChemoPPView diagnostics

#ifdef _DEBUG
void CChemoPPView::AssertValid() const
{
	CView::AssertValid();
}

void CChemoPPView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CChemoPPDoc* CChemoPPView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CChemoPPDoc)));
	return (CChemoPPDoc*)m_pDocument;
}
#endif //_DEBUG


// CChemoPPView message handlers
