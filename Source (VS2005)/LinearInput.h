#pragma once

#include "afxwin.h"
#include "Calc/LinearPP.h"


// CLinearInput dialog

class CLinearInput : public CPropertyPage
{
	DECLARE_DYNAMIC(CLinearInput)

public:
	CLinearInput();
	virtual ~CLinearInput();

// Dialog Data
	enum { IDD = IDD_LINEAR_INPUT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	LinearPP h;
	BOOL Open(CString lpszPathName);
	BOOL Save(CString lpszPathName);
	void Read();
	virtual void Compute(vector<double>& Pdata, vector<double>& tdata);

public:
	BOOL OnInitDialog();
	void ComboBoxFill();

	CComboBox cLinearLength;
	CComboBox cLinearC0;
	CComboBox cLinearP0;
	CComboBox cLinearCf;
	CComboBox cLinearTemp;
	CComboBox cLinearTime;
	CComboBox cLinearCdf;
	CComboBox cLinearPdf;
	CComboBox cLinearDt;
	CComboBox cLinearDx;
	
	CEdit tLinearLength;
	CEdit tLinearC0;
	CEdit tLinearP0;
	CEdit tLinearCf;
	CEdit tLinearNions;
	CEdit tLinearMw;
	CEdit tLinearTemp;
	CEdit tLinearTime;
	CEdit tLinearCdf;
	CEdit tLinearPdf;
	CEdit tLinearDt;
	CEdit tLinearDx;
	CEdit tLinearK1;
	CEdit tLinearK2;
	CEdit tLinearDeff;
public:
	afx_msg void OnUpdateFileReadexperimentdata(CCmdUI *pCmdUI);
public:
	afx_msg void OnFileData();
};
