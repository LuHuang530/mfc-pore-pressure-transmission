// ChemoPPDoc.cpp : implementation of the CChemoPPDoc class
//

#include "stdafx.h"
#include "ChemoPP.h"

#include "ChemoPPDoc.h"

#include "MainFrm.h"
#include "ChildFrm.h"
#include "TabView.h"
#include "TabSheet.h"
#include "LinearInput.h"
#include "RadialInput.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CChemoPPDoc

IMPLEMENT_DYNCREATE(CChemoPPDoc, CDocument)

BEGIN_MESSAGE_MAP(CChemoPPDoc, CDocument)
END_MESSAGE_MAP()


// CChemoPPDoc construction/destruction

CChemoPPDoc::CChemoPPDoc()
{
	// TODO: add one-time construction code here

}

CChemoPPDoc::~CChemoPPDoc()
{
}

BOOL CChemoPPDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}




// CChemoPPDoc serialization

void CChemoPPDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}


// CChemoPPDoc diagnostics

#ifdef _DEBUG
void CChemoPPDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CChemoPPDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG


// CChemoPPDoc commands

//Override Save
BOOL CChemoPPDoc::OnSaveDocument(LPCTSTR lpszPathName)
{
	CDocument::OnSaveDocument(lpszPathName);
	
	//find active PropertyPage (linear or radial input)
	CChildFrame * pChild = (CChildFrame*)(((CMainFrame*)AfxGetMainWnd())->MDIGetActive());
	CTabView* pForm = (CTabView*)pChild->m_wndSplitter.GetPane(0,0);
	CTabSheet* pTab = (CTabSheet*)pForm->m_Tab;
	CPropertyPage* pPage = pTab->GetActivePage();

	CLinearInput* pLinear = &(pTab->m_Linear);
	CRadialInput* pRadial = &(pTab->m_Radial);

	if (pPage)
	{
		if (pPage == pLinear) return pLinear->Save(lpszPathName);
		if (pPage == pRadial) return pRadial->Save(lpszPathName);
		else return FALSE;
	}
	else
		return FALSE;
}


void CChemoPPDoc::OnFileSaveAs()
{
	// TODO: Add your command handler code here
	LPCTSTR pszFilter = _T("Input files (*.in)|*.in| All files (*.*)|*.*||");

	CFileDialog dlgFile(FALSE, _T("in"), _T("*.in"), OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, pszFilter, AfxGetMainWnd());

	if(dlgFile.DoModal() == IDOK)
	{
		CString lpszPathName = dlgFile.GetPathName();
		OnSaveDocument(lpszPathName);
		SetPathName(lpszPathName);
		SetTitle(dlgFile.GetFileTitle());
	}
}

void CChemoPPDoc::OnFileSave()
{
	// TODO: Add your command handler code here
	CFileStatus status;
   
	if(! CFile::GetStatus(GetPathName(), status))  
		OnFileSaveAs();
	else
	{
	  if (IsModified()) 
		  OnSaveDocument(GetPathName()); 
	}
}