#pragma once

#include "afxwin.h"
#include "Calc/RadialPP.h"


// CRadialInput dialog

class CRadialInput : public CPropertyPage
{
	DECLARE_DYNAMIC(CRadialInput)

public:
	CRadialInput();
	virtual ~CRadialInput();

// Dialog Data
	enum { IDD = IDD_RADIAL_INPUT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	RadialPP h;
	BOOL Open(CString lpszPathName);
	BOOL Save(CString lpszPathName);
	void Read();
	virtual void Compute();

public:
	BOOL OnInitDialog();
	void ComboBoxFill();

	CComboBox cRadialRw;
	CComboBox cRadialRe;
	CComboBox cRadialRinf;
	CComboBox cRadialC0;
	CComboBox cRadialP0;
	CComboBox cRadialCf;
	CComboBox cRadialTemp;
	CComboBox cRadialTime;
	CComboBox cRadialCdf;
	CComboBox cRadialPw;
	CComboBox cRadialDt;
	CComboBox cRadialDr;
	
	CEdit tRadialRw;
	CEdit tRadialRe;
	CEdit tRadialRinf;
	CEdit tRadialC0;
	CEdit tRadialP0;
	CEdit tRadialCf;
	CEdit tRadialNions;
	CEdit tRadialMw;
	CEdit tRadialTemp;
	CEdit tRadialTime;
	CEdit tRadialCdf;
	CEdit tRadialPw;
	CEdit tRadialDt;
	CEdit tRadialDr;
	CEdit tRadialK1;
	CEdit tRadialK2;
	CEdit tRadialL1;
	CEdit tRadialL2;

public:
	afx_msg void OnFileReadexperimentdata();
};
