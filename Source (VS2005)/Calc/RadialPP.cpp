//RadialPP.cpp: implementation file
//

#include "stdafx.h"
#include "RadialPP.h"
#include <fstream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <exception>
#include <stdexcept>
#include "../general.h"
#include "../Unit/Unit.h"

using namespace std;



RadialPP::RadialPP()
{

}

RadialPP::~RadialPP()
{

}

void RadialPP::Compute(const RadialPP& h) //compute pressure profile, using SI units
{
	RadialPP tmp;
	tmp = tmp.ToSI(h);
	const double R = 8314.0; //(J/Kmol.K)
	
	//check for data validation
	if (tmp.t <=0 || tmp.dt <= 0 || tmp.Rw <= 0 || tmp.Re <= tmp.Rw || tmp.Rinf < tmp.Re || tmp.dr <= 0
		|| tmp.cf <= 0 || tmp.M <= 0 || tmp.C0 < 0 || tmp.Cdf < 0 || tmp.P0 < 0 || tmp.Pw < 0 || tmp.n < 0)
		throw runtime_error("Invalid input");
	
	//determine number of steps (uniform)
	double tN = ceil(tmp.t / tmp.dt);
	double rN = ceil((tmp.Re - tmp.Rw) / tmp.dr);
	
	//resize step size
	tmp.dt = tmp.t / tN;
	tmp.dr = (tmp.Re - tmp.Rw) / rN;
	
	long tNum = (long)tN;
	long rNum = (long)rN;
	long rNum2 = (long)ceil((tmp.Rinf - tmp.Rw) / tmp.dr);

	//max number of plot points to record
	long nPlot = 1000; //create a dialog box for this (charting part)
	long nLo = (long)floor((double)tNum / ((double)nPlot - 1.0));
	if (nLo == 0) nLo = 1;
	long nHi = (long)ceil((double)tNum / ((double)nPlot - 1.0));
	long nRec = nLo;

	
	//computation begins
	vector<vector<double> > C;
	vector<vector<double> > P;
	vector<double> tdata; //data for plots
	vector<double> Pdata; //data for plots

	try
	{
		C = vector<vector<double> > (2, vector<double> (rNum2+1));
		P = vector<vector<double> > (2, vector<double> (rNum2+1));
	}
	catch (bad_alloc& e)
	{
		throw e;
	}
	
	//initial condition
	for (long j=0; j<=rNum2; ++j)
	{
		P.at(0).at(j) = tmp.P0;
		C.at(0).at(j) = tmp.C0;
	}
	
	//record data at point of interest
	tdata.push_back(0);
	Pdata.push_back(P.at(0).at(rNum));
	
	double A1 = tmp.dt * tmp.n * R * tmp.Temp * tmp.L2;
	double A2 = tmp.dt * tmp.L1;
	double A3 = tmp.dt * tmp.K1 / tmp.cf;
	double A4 = tmp.dt * tmp.n * R * tmp.Temp * tmp.K2 / tmp.cf;
	
	int i1 = 1, i0 = 0; //row index to interchange (alternating value)

	for (long i=1; i<=tNum; ++i)
	{
		//borehole wall boundary condition
		C.at(i1).at(0) = tmp.Cdf / tmp.M;
		P.at(i1).at(0) = tmp.Pw;
		
		//intermediate points + point of interest
		for (long j=1; j<rNum2; ++j)
		{
			double C1 = (C.at(i0).at(j+1) - 2*C.at(i0).at(j) + C.at(i0).at(j-1)) / pow(tmp.dr,2);
			double C2 = 1 / (tmp.Rw + j*tmp.dr) * (C.at(i0).at(j+1) - C.at(i0).at(j)) / tmp.dr;

			double P1 = (P.at(i0).at(j+1) - 2*P.at(i0).at(j) + P.at(i0).at(j-1)) / pow(tmp.dr,2);
			double P2 = 1 / (tmp.Rw + j*tmp.dr) * (P.at(i0).at(j+1) - P.at(i0).at(j)) / tmp.dr;
			
			C.at(i1).at(j) = C.at(i0).at(j) + A1 * (C1 + C2) + A2 * (P1 + P2);
			P.at(i1).at(j) = P.at(i0).at(j) + A3 * (P1 + P2) + A4 * (C1 + C2);
		}

		//semi-infinite boundary condition
		C.at(i1).at(rNum2) = tmp.C0;
		P.at(i1).at(rNum2) = tmp.P0;
		
		//record data at point of interest
		if (i == nRec)
		{
			tdata.push_back(i*tmp.dt);
			Pdata.push_back(P.at(i1).at(rNum));

			nRec += nHi;
			swap(nLo, nHi);
		}

		//switch row index
		swap(i1, i0);
	}

	//check last point at tNum
	if ((nRec-nLo) != tNum)
	{
		tdata.push_back(tNum*tmp.dt);
		Pdata.push_back(P.at(i0).at(rNum));
	}


	//output to file, convert back to user chosen units
	UnitCollection* ptr = UnitCollection::Instance("Units.txt"); //initialize
	Unit *pUt, *pUP;
	int ErrStatus = 0;

	pUt = ptr->GetUnit("Time");
	pUP = ptr->GetUnit("Pressure");

	ofstream outfile("PvstRadial.out");
	vector<double>::const_iterator iP;
	vector<double>::const_iterator it;
	
	outfile << "#t\tP" << endl;
	outfile << "\"" << h.ut << "\"\t\"" << h.uP0 << "\"" << endl;
	for (iP=Pdata.begin(), it=tdata.begin(); iP!=Pdata.end(); ++iP, ++it)
		outfile << pUt->Convert((*it), tmp.ut, h.ut, ErrStatus) << "\t" << pUP->Convert((*iP), tmp.uP0, h.uP0, ErrStatus) << endl;

	outfile.close();
}

RadialPP RadialPP::ToSI(const RadialPP& h) //convert all parameters to SI units
{
	RadialPP tmp;

	UnitCollection* ptr = UnitCollection::Instance("Units.txt"); //initialize
	Unit* pU;
	int ErrStatus = 0;

	//length unit type
	pU = ptr->GetUnit("Length");
	tmp.Rw = pU->ToSI(h.Rw, h.uRw, ErrStatus);
	tmp.Re = pU->ToSI(h.Re, h.uRe, ErrStatus);
	tmp.Rinf = pU->ToSI(h.Rinf, h.uRinf, ErrStatus);
	tmp.dr = pU->ToSI(h.dr, h.udr, ErrStatus);
	tmp.uRw = tmp.uRe = tmp.uRinf = tmp.udr = pU->GetSI();

	//pressure unit type
	pU = ptr->GetUnit("Pressure");
	tmp.P0 = pU->ToSI(h.P0, h.uP0, ErrStatus);
	tmp.Pw = pU->ToSI(h.Pw, h.uPw, ErrStatus);
	tmp.uP0 = tmp.uPw = pU->GetSI();

	//concentration unit type
	pU = ptr->GetUnit("Concentration");
	tmp.Cdf = pU->ToSI(h.Cdf, h.uCdf, ErrStatus);
	tmp.uCdf = pU->GetSI();

	//concentration (mol basis) unit type
	pU = ptr->GetUnit("Concentration (mol basis)");
	tmp.C0 = pU->ToSI(h.C0, h.uC0, ErrStatus);
	tmp.uC0 = pU->GetSI();

	//compressibility unit type
	pU = ptr->GetUnit("Compressibility");
	tmp.cf = pU->ToSI(h.cf, h.ucf, ErrStatus);
	tmp.ucf = pU->GetSI();

	//temperature unit type
	pU = ptr->GetUnit("Temperature");
	tmp.Temp = pU->ToSI(h.Temp, h.uTemp, ErrStatus);
	tmp.uTemp = pU->GetSI();

	//time unit type
	pU = ptr->GetUnit("Time");
	tmp.t = pU->ToSI(h.t, h.ut, ErrStatus);
	tmp.ut = pU->GetSI();
	tmp.dt = pU->ToSI(h.dt, h.udt, ErrStatus);
	tmp.udt = pU->GetSI();

	//unchange
	tmp.n = h.n;
	tmp.M = h.M;
	tmp.K1 = h.K1;
	tmp.K2 = h.K2;
	tmp.L1 = h.L1;
	tmp.L2 = h.L2;

	return tmp;
}

ostream& operator << (ostream& outs, const RadialPP& h)
{
	outs << "\"Radial\"" << endl;
	outs << "#Input Data" << endl;

	outs << h.Rw << "\t\"" << h.uRw << "\"\t" << "#wellbore radius" << endl;
	outs << h.Re << "\t\"" << h.uRe << "\"\t" << "#radius of investigation" << endl;
	outs << h.Rinf << "\t\"" << h.uRinf << "\"\t" << "#infinite radius" << endl;
	outs << h.C0 << "\t\"" << h.uC0 << "\"\t" << "#initial pore fluid concentration (mol basis)" << endl;
	outs << h.P0 << "\t\"" << h.uP0 << "\"\t" << "#initial pore pressure" << endl;
	outs << h.cf << "\t\"" << h.ucf << "\"\t" << "#pore fluid compressibility" << endl;

	outs << h.Cdf << "\t\"" << h.uCdf << "\"\t" << "#drilling fluid concentration (mass basis)" << endl;
	outs << h.Pw << "\t\"" << h.uPw << "\"\t" << "#drilling fluid pressure" << endl;

	outs << h.n << "\t\"\"\t" << "#number of constituent ions of dissociating solute" << endl;
	outs << h.M << "\t\"\"\t" << "#molecular weight" << endl;
	outs << h.Temp << "\t\"" << h.uTemp << "\"\t" << "#temperature" << endl;
	outs << h.t << "\t\"" << h.ut << "\"\t" << "#time" << endl;

	outs << h.dt << "\t\"" << h.udt << "\"\t" << "#(max) time step size" << endl;
	outs << h.dr << "\t\"" << h.udr << "\"\t" << "#(max) length step size" << endl;

	outs << h.K1 << "\t\"m3.s/kg\"\t" << "#permeability" << endl;
	outs << h.K2 << "\t\"m3.s/kg\"\t" << "#membrane efficiency" << endl;
	outs << h.L1 << "\t\"m3.s/kg\"\t" << "#LI" << endl;
	outs << h.L2 << "\t\"m3.s/kg\"\t" << "#LII" << endl;
	
	return outs;
}

istream& operator >>(istream& ins, RadialPP& h)
{
	ReadAString(ins); //ignore input mode

	BypassComment(ins);	ins >> h.Rw;
	BypassComment(ins);	h.uRw = ReadAString(ins);
	BypassComment(ins);	ins >> h.Re;
	BypassComment(ins);	h.uRe = ReadAString(ins);
	BypassComment(ins);	ins >> h.Rinf;
	BypassComment(ins);	h.uRinf = ReadAString(ins);
	BypassComment(ins);	ins >> h.C0;
	BypassComment(ins);	h.uC0 = ReadAString(ins);
	BypassComment(ins);	ins >> h.P0;
	BypassComment(ins);	h.uP0 = ReadAString(ins);
	BypassComment(ins);	ins >> h.cf;
	BypassComment(ins);	h.ucf = ReadAString(ins);

	BypassComment(ins);	ins >> h.Cdf;
	BypassComment(ins);	h.uCdf = ReadAString(ins);
	BypassComment(ins);	ins >> h.Pw;
	BypassComment(ins);	h.uPw = ReadAString(ins);

	BypassComment(ins);	ins >> h.n;
	ReadAString(ins);
	BypassComment(ins);	ins >> h.M;
	ReadAString(ins);
	BypassComment(ins);	ins >> h.Temp;
	BypassComment(ins);	h.uTemp = ReadAString(ins);
	BypassComment(ins);	ins >> h.t;
	BypassComment(ins);	h.ut = ReadAString(ins);

	BypassComment(ins);	ins >> h.dt;
	BypassComment(ins);	h.udt = ReadAString(ins);
	BypassComment(ins);	ins >> h.dr;
	BypassComment(ins);	h.udr = ReadAString(ins);

	BypassComment(ins);	ins >> h.K1;
	ReadAString(ins);
	BypassComment(ins);	ins >> h.K2;
	ReadAString(ins);
	BypassComment(ins);	ins >> h.L1;
	ReadAString(ins);
	BypassComment(ins);	ins >> h.L2;
	//ReadAString(ins);

	return ins;
}