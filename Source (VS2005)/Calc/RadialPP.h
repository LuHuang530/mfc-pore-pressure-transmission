//RadialPP.h
//

#ifndef RADIALPP_H
#define RADIALPP_H

#include <string>

using namespace std;



class RadialPP
{
public:
	RadialPP();
	~RadialPP();

	void Compute(const RadialPP& h); //compute pressure profile, using SI units
	RadialPP ToSI(const RadialPP& h); //convert all parameters to SI units

public:
	//core sample parameters
	double Rw; //wellbore radius
	string uRw; //unit
	double Re; //radius of investigation (interest)
	string uRe;
	double Rinf; //infinite radius
	string uRinf;
	double C0; //initial pore fluid concentration (mol basis)
	string uC0;
	double P0; //initial pore pressure
	string uP0;
	double cf; //pore fluid compressibility
	string ucf;
	
	//drilling fluid parameters
	double Cdf; //drilling fluid concentration (mass basis)
	string uCdf;
	double Pw; //drilling fluid (wellbore) pressure
	string uPw;
	
	//other parameters
	int n; //number of constituent ions of dissociating solute
	double M; //molecular weight
	double Temp; //temperature
	string uTemp;
	double t; //time
	string ut;
	
	//numerical parameters
	double dt; //(max) time step size
	string udt;
	double dr; //(max) length step size
	string udr;
	
	//misc parameters
	double K1; //SIUnit
	double K2; //SIUnit
	double L1; //SIUnit
	double L2; //SIUnit
	
protected:
	//input and output
	friend ostream& operator << (ostream& outs, const RadialPP& h);
	friend istream& operator >>(istream& ins, RadialPP& h);

};

#endif