//LinearPP.cpp: implementation file
//

#include "stdafx.h"
#include "LinearPP.h"
#include <fstream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <exception>
#include <stdexcept>
#include "../general.h"
#include "../Unit/Unit.h"

using namespace std;



LinearPP::LinearPP()
{

}

LinearPP::~LinearPP()
{

}

void LinearPP::Compute(const LinearPP& h, vector<double>& Pdata, vector<double>& tdata) //compute pressure profile, using SI units
{
	LinearPP tmp; 
	tmp = tmp.ToSI(h);
	const double R = 8314.0; //(J/Kmol.K)
	
	//check for data validation
	if (tmp.t <=0 || tmp.dt <= 0 || tmp.L <= 0 || tmp.dx <= 0 || tmp.cf <= 0 || tmp.M <= 0 
		|| tmp.C0 < 0 || tmp.Cdf < 0 || tmp.P0 < 0 || tmp.Pdf < 0 || tmp.n < 0)
		throw runtime_error("Invalid input");
	
	//determine number of steps (uniform)
	double tN = ceil(tmp.t / tmp.dt);
	double xN = ceil(tmp.L / tmp.dx);
	
	//resize step size
	tmp.dt = tmp.t / tN;
	tmp.dx = tmp.L / xN;

	long tNum = (long)tN;
	long xNum = (long)xN;

	//max number of plot points to record
	if (h.nPlot<2) nPlot = 100;
	long nLo = (long)floor((double)tNum / ((double)h.nPlot - 1.0));
	if (nLo == 0) nLo = 1;
	long nHi = (long)ceil((double)tNum / ((double)h.nPlot - 1.0));
	long nRec = nLo;

	//computation begins
	vector<vector<double> > C;
	vector<vector<double> > P;


	try
	{
		C = vector<vector<double> > (2, vector<double> (xNum+1));
		P = vector<vector<double> > (2, vector<double> (xNum+1));
	}
	catch (bad_alloc& e)
	{
		throw e;
	}
	
	//initial condition
	for (long j=0; j<=xNum; ++j)
	{
		P.at(0).at(j) = tmp.P0;
		C.at(0).at(j) = tmp.C0;
	}

	tdata.push_back(0);
	Pdata.push_back(P.at(0).at(xNum));
	
	double A1 = tmp.dt * tmp.Deff / pow(tmp.dx,2);
	double A2 = tmp.dt * tmp.K1 / tmp.cf / pow(tmp.dx,2);
	double A3 = tmp.dt * tmp.n * R * tmp.Temp * tmp.K2 / tmp.cf / pow(tmp.dx,2);
	
	int i1 = 1, i0 = 0; //row index to interchange (alternating value)

	for (long i=1; i<=tNum; ++i)
	{
		//entry boundary condition
		C.at(i1).at(0) = tmp.Cdf / tmp.M;
		P.at(i1).at(0) = tmp.Pdf;
		
		//intermediate points
		for (long j=1; j<xNum; ++j)
		{
			double A4 = C.at(i0).at(j+1) - 2*C.at(i0).at(j) + C.at(i0).at(j-1);
			double A5 = P.at(i0).at(j+1) - 2*P.at(i0).at(j) + P.at(i0).at(j-1);

			C.at(i1).at(j) = C.at(i0).at(j) + A1 * A4;
			P.at(i1).at(j) = P.at(i0).at(j) + A2 * A5 + A3 * A4;
		}

		//no-flow boundary condition
		C.at(i1).at(xNum) = C.at(i1).at(xNum-1);
		P.at(i1).at(xNum) = P.at(i1).at(xNum-1);
		
		//write to data list
		if (i == nRec)
		{
			tdata.push_back(i*tmp.dt);
			Pdata.push_back(P.at(i1).at(xNum));

			nRec += nHi;
			swap(nLo, nHi);
		}

		//switch row index
		swap(i1, i0);
	}

	//check last point at tNum
	if ((nRec-nLo) != tNum)
	{
		tdata.push_back(tNum*tmp.dt);
		Pdata.push_back(P.at(i0).at(xNum));
	}


	//output to file, convert back to user chosen units
	UnitCollection* ptr = UnitCollection::Instance("Units.txt"); //initialize
	Unit *pUt, *pUP;
	int ErrStatus = 0;

	pUt = ptr->GetUnit("Time");
	pUP = ptr->GetUnit("Pressure");

	ofstream outfile("PvstLinear.out");
	vector<double>::const_iterator iP;
	vector<double>::const_iterator it;
	
	outfile << "#t\tP" << endl;
	outfile << "\"" << h.ut << "\"\t\"" << h.uP0 << "\"" << endl;
	for (iP=Pdata.begin(), it=tdata.begin(); iP!=Pdata.end(); ++iP, ++it)
		outfile << pUt->Convert((*it), tmp.ut, h.ut, ErrStatus) << "\t" << pUP->Convert((*iP), tmp.uP0, h.uP0, ErrStatus) << endl;

	outfile.close();
}

LinearPP LinearPP::ToSI(const LinearPP& h) //convert all parameters to SI units
{
	LinearPP tmp;

	UnitCollection* ptr = UnitCollection::Instance("Units.txt"); //initialize
	Unit* pU;
	int ErrStatus = 0;

	//length unit type
	pU = ptr->GetUnit("Length");
	tmp.L = pU->ToSI(h.L, h.uL, ErrStatus);
	tmp.dx = pU->ToSI(h.dx, h.udx, ErrStatus);
	tmp.uL = tmp.udx = pU->GetSI();

	//pressure unit type
	pU = ptr->GetUnit("Pressure");
	tmp.P0 = pU->ToSI(h.P0, h.uP0, ErrStatus);
	tmp.Pdf = pU->ToSI(h.Pdf, h.uPdf, ErrStatus);
	tmp.uP0 = tmp.uPdf = pU->GetSI();

	//concentration unit type
	pU = ptr->GetUnit("Concentration");
	tmp.Cdf = pU->ToSI(h.Cdf, h.uCdf, ErrStatus);
	tmp.uCdf = pU->GetSI();

	//concentration (mol basis) unit type
	pU = ptr->GetUnit("Concentration (mol basis)");
	tmp.C0 = pU->ToSI(h.C0, h.uC0, ErrStatus);
	tmp.uC0 = pU->GetSI();

	//compressibility unit type
	pU = ptr->GetUnit("Compressibility");
	tmp.cf = pU->ToSI(h.cf, h.ucf, ErrStatus);
	tmp.ucf = pU->GetSI();

	//temperature unit type
	pU = ptr->GetUnit("Temperature");
	tmp.Temp = pU->ToSI(h.Temp, h.uTemp, ErrStatus);
	tmp.uTemp = pU->GetSI();

	//time unit type
	pU = ptr->GetUnit("Time");
	tmp.t = pU->ToSI(h.t, h.ut, ErrStatus);
	tmp.ut = pU->GetSI();
	tmp.dt = pU->ToSI(h.dt, h.udt, ErrStatus);
	tmp.udt = pU->GetSI();

	//unchange
	tmp.n = h.n;
	tmp.M = h.M;
	tmp.K1 = h.K1;
	tmp.K2 = h.K2;
	tmp.Deff = h.Deff;

	return tmp;
}

ostream& operator << (ostream& outs, const LinearPP& h)
{
	outs << "\"Linear\"" << endl;
	outs << "#Input Data" << endl;

	outs << h.L << "\t\"" << h.uL << "\"\t" << "#core sample length" << endl;
	outs << h.C0 << "\t\"" << h.uC0 << "\"\t" << "#initial pore fluid concentration (mol basis)" << endl;
	outs << h.P0 << "\t\"" << h.uP0 << "\"\t" << "#initial pore pressure" << endl;
	outs << h.cf << "\t\"" << h.ucf << "\"\t" << "#pore fluid compressibility" << endl;

	outs << h.Cdf << "\t\"" << h.uCdf << "\"\t" << "#test fluid concentration (mass basis)" << endl;
	outs << h.Pdf << "\t\"" << h.uPdf << "\"\t" << "#test fluid pressure" << endl;

	outs << h.n << "\t\"\"\t" << "#number of constituent ions of dissociating solute" << endl;
	outs << h.M << "\t\"\"\t" << "#molecular weight" << endl;
	outs << h.Temp << "\t\"" << h.uTemp << "\"\t" << "#temperature" << endl;
	outs << h.t << "\t\"" << h.ut << "\"\t" << "#time" << endl;

	outs << h.dt << "\t\"" << h.udt << "\"\t" << "#(max) time step size" << endl;
	outs << h.dx << "\t\"" << h.udx << "\"\t" << "#(max) length step size" << endl;

	outs << h.K1 << "\t\"m3.s/kg\"\t" << "#permeability" << endl;
	outs << h.K2 << "\t\"m3.s/kg\"\t" << "#membrane efficiency" << endl;
	outs << h.Deff << "\t\"m2/s\"\t" << "#effective solute diffusion coefficient" << endl;
	
	return outs;
}

istream& operator >>(istream& ins, LinearPP& h)
{
	ReadAString(ins); //ignore input mode

	BypassComment(ins);	ins >> h.L;
	BypassComment(ins);	h.uL = ReadAString(ins);
	BypassComment(ins);	ins >> h.C0;
	BypassComment(ins);	h.uC0 = ReadAString(ins);
	BypassComment(ins);	ins >> h.P0;
	BypassComment(ins);	h.uP0 = ReadAString(ins);
	BypassComment(ins);	ins >> h.cf;
	BypassComment(ins);	h.ucf = ReadAString(ins);

	BypassComment(ins);	ins >> h.Cdf;
	BypassComment(ins);	h.uCdf = ReadAString(ins);
	BypassComment(ins);	ins >> h.Pdf;
	BypassComment(ins);	h.uPdf = ReadAString(ins);

	BypassComment(ins);	ins >> h.n;
	ReadAString(ins);
	BypassComment(ins);	ins >> h.M;
	ReadAString(ins);
	BypassComment(ins);	ins >> h.Temp;
	BypassComment(ins);	h.uTemp = ReadAString(ins);
	BypassComment(ins);	ins >> h.t;
	BypassComment(ins);	h.ut = ReadAString(ins);

	BypassComment(ins);	ins >> h.dt;
	BypassComment(ins);	h.udt = ReadAString(ins);
	BypassComment(ins);	ins >> h.dx;
	BypassComment(ins);	h.udx = ReadAString(ins);

	BypassComment(ins);	ins >> h.K1;
	ReadAString(ins);
	BypassComment(ins);	ins >> h.K2;
	ReadAString(ins);
	BypassComment(ins);	ins >> h.Deff;
	//ReadAString(ins);

	return ins;
}