//LinearPP.h
//

#ifndef LINEARPP_H
#define LINEARPP_H

#include <string>
#include <vector>

using namespace std;



class LinearPP
{
public:
	LinearPP();
	~LinearPP();

	void Compute(const LinearPP& h, vector<double>& Pdata, vector<double>& tdata); //compute pressure profile, using SI units
	LinearPP ToSI(const LinearPP& h); //convert all parameters to SI units

public:
	//core sample parameters
	double L; //core sample length
	string uL; //unit
	double C0; //initial pore fluid concentration (mol basis)
	string uC0;
	double P0; //initial pore pressure
	string uP0;
	double cf; //pore fluid compressibility
	string ucf;
	
	//test fluid parameters
	double Cdf; //test fluid concentration (mass basis)
	string uCdf;
	double Pdf; //test fluid pressure
	string uPdf;
	
	//other parameters
	int n; //number of constituent ions of dissociating solute
	double M; //molecular weight
	double Temp; //temperature
	string uTemp;
	double t; //time
	string ut;
	
	//numerical parameters
	double dt; //(max) time step size
	string udt;
	double dx; //(max) length step size
	string udx;
	
	//misc parameters (data fit)
	double K1; //"permeability"									SIUnit
	double K2; //"membrane efficiency"							SIUnit
	double Deff; //effective solute diffusion coefficient		SIUnit

	//max number of data points recorded for plot/chart
	long nPlot;
	
protected:
	//input and output
	friend ostream& operator << (ostream& outs, const LinearPP& h);
	friend istream& operator >>(istream& ins, LinearPP& h);

};

#endif