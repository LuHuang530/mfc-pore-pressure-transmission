// RadialInput.cpp : implementation file
//

#include "stdafx.h"
#include "ChemoPP.h"
#include "RadialInput.h"
#include "Unit/Unit.h"
#include "Calc/RadialPP.h"
#include "ChildFrm.h"
#include "MainFrm.h"
#include "TabView.h"
// CRadialInput dialog

IMPLEMENT_DYNAMIC(CRadialInput, CPropertyPage)

CRadialInput::CRadialInput()
	: CPropertyPage(CRadialInput::IDD)
{

}

CRadialInput::~CRadialInput()
{
}

void CRadialInput::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO1, cRadialRw);
	DDX_Control(pDX, IDC_COMBO10, cRadialRe);
	DDX_Control(pDX, IDC_COMBO13, cRadialRinf);
	DDX_Control(pDX, IDC_COMBO12, cRadialC0);
	DDX_Control(pDX, IDC_COMBO3, cRadialP0);
	DDX_Control(pDX, IDC_COMBO4, cRadialCf);
	DDX_Control(pDX, IDC_COMBO2, cRadialTemp);
	DDX_Control(pDX, IDC_COMBO9, cRadialTime);
	DDX_Control(pDX, IDC_COMBO5, cRadialCdf);
	DDX_Control(pDX, IDC_COMBO6, cRadialPw);
	DDX_Control(pDX, IDC_COMBO7, cRadialDt);
	DDX_Control(pDX, IDC_COMBO8, cRadialDr);

	DDX_Control(pDX, IDC_EDIT1, tRadialRw);
	DDX_Control(pDX, IDC_EDIT16, tRadialRe);
	DDX_Control(pDX, IDC_EDIT18, tRadialRinf);
	DDX_Control(pDX, IDC_EDIT2, tRadialC0);
	DDX_Control(pDX, IDC_EDIT3, tRadialP0);
	DDX_Control(pDX, IDC_EDIT4, tRadialCf);
	DDX_Control(pDX, IDC_EDIT7, tRadialNions);
	DDX_Control(pDX, IDC_EDIT8, tRadialMw);
	DDX_Control(pDX, IDC_EDIT12, tRadialTemp);
	DDX_Control(pDX, IDC_EDIT13, tRadialTime);
	DDX_Control(pDX, IDC_EDIT5, tRadialCdf);
	DDX_Control(pDX, IDC_EDIT6, tRadialPw);
	DDX_Control(pDX, IDC_EDIT14, tRadialDt);
	DDX_Control(pDX, IDC_EDIT15, tRadialDr);
	DDX_Control(pDX, IDC_EDIT9, tRadialK1);
	DDX_Control(pDX, IDC_EDIT10, tRadialK2);
	DDX_Control(pDX, IDC_EDIT11, tRadialL1);
	DDX_Control(pDX, IDC_EDIT17, tRadialL2);	
}

BOOL CRadialInput::OnInitDialog()
{
	CPropertyPage::OnInitDialog();
	ComboBoxFill();

	return TRUE;
}

void CRadialInput::Compute()
{
	CRadialInput::Read();
	h.Compute(h);
}

void CRadialInput::ComboBoxFill()
{
	UnitCollection* ptr = UnitCollection::Instance("Units.txt"); //initialize
	
	//populating comboboxes
	Unit* pU;
	const vector<Unit_Convertor>* pUl;
	vector<Unit_Convertor>::const_iterator i;
	std::string s;
	CString cs;

	//length unit type
	pU = ptr->GetUnit("Length");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		s = (*i).Unit_Name;
		cs = s.c_str();
		
		cRadialRw.AddString(cs); //wellbore radius
		cRadialRe.AddString(cs); //radius of investigation
		cRadialRinf.AddString(cs); //infinite radius
		cRadialDr.AddString(cs); //dr
	}
	
	//pressure unit type
	pU = ptr->GetUnit("Pressure");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		s = (*i).Unit_Name;
		cs = s.c_str();
		
		cRadialP0.AddString(cs); //initial pore pressure
		cRadialPw.AddString(cs); //drilling fluid pressure
	}

	//concentration (mass basis) unit type
	pU = ptr->GetUnit("Concentration");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		s = (*i).Unit_Name;
		cs = s.c_str();
		
		cRadialCdf.AddString(cs); //drilling fluid concentration
	}

	//concentration (mol basis) unit type
	pU = ptr->GetUnit("Concentration (mol basis)");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		s = (*i).Unit_Name;
		cs = s.c_str();
		
		cRadialC0.AddString(cs); //initial pore fluid concentration
	}

	//compressibility unit type
	pU = ptr->GetUnit("Compressibility");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		s = (*i).Unit_Name;
		cs = s.c_str();
		
		cRadialCf.AddString(cs); //pore fluid compressibility
	}

	//temperature unit type
	pU = ptr->GetUnit("Temperature");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		s = (*i).Unit_Name;
		cs = s.c_str();
		
		cRadialTemp.AddString(cs); //temperature
	}

	//time unit type
	pU = ptr->GetUnit("Time");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		s = (*i).Unit_Name;
		cs = s.c_str();
		
		cRadialTime.AddString(cs); //total time
		cRadialDt.AddString(cs); //dt
	}
}

BOOL CRadialInput::Open(CString lpszPathName)
{
	//read from file
	ifstream ins(lpszPathName);
	ins >> h;
	ins.close();

	CString cs;

	cs.Format(_T("%.3f"), h.Rw);
	tRadialRw.SetWindowTextW(cs);
	cs = (h.uRw).c_str();
	cRadialRw.SelectString(-1,cs);

	cs.Format(_T("%.3f"), h.Re);
	tRadialRe.SetWindowTextW(cs);
	cs = (h.uRe).c_str();
	cRadialRe.SelectString(-1,cs);

	cs.Format(_T("%.3f"), h.Rinf);
	tRadialRinf.SetWindowTextW(cs);
	cs = (h.uRinf).c_str();
	cRadialRinf.SelectString(-1,cs);

	cs.Format(_T("%.2f"), h.C0);
	tRadialC0.SetWindowTextW(cs);
	cs = (h.uC0).c_str();
	cRadialC0.SelectString(-1,cs);

	cs.Format(_T("%.2f"), h.P0);
	tRadialP0.SetWindowTextW(cs);
	cs = (h.uP0).c_str();
	cRadialP0.SelectString(-1,cs);

	cs.Format(_T("%.2e"), h.cf);
	tRadialCf.SetWindowTextW(cs);
	cs = (h.ucf).c_str();
	cRadialCf.SelectString(-1,cs);

	cs.Format(_T("%i"), h.n);
	tRadialNions.SetWindowTextW(cs);

	cs.Format(_T("%.2f"), h.M);
	tRadialMw.SetWindowTextW(cs);

	cs.Format(_T("%.2f"), h.Temp);
	tRadialTemp.SetWindowTextW(cs);	
	cs = (h.uTemp).c_str();
	cRadialTemp.SelectString(-1,cs);

	cs.Format(_T("%.1f"), h.t);
	tRadialTime.SetWindowTextW(cs);	
	cs = (h.ut).c_str();
	cRadialTime.SelectString(-1,cs);

	cs.Format(_T("%.2f"), h.Cdf);
	tRadialCdf.SetWindowTextW(cs);
	cs = (h.uCdf).c_str();
	cRadialCdf.SelectString(-1,cs);

	cs.Format(_T("%.2f"), h.Pw);
	tRadialPw.SetWindowTextW(cs);
	cs = (h.uPw).c_str();
	cRadialPw.SelectString(-1,cs);

	cs.Format(_T("%.1f"), h.dt);
	tRadialDt.SetWindowTextW(cs);
	cs = (h.udt).c_str();
	cRadialDt.SelectString(-1,cs);

	cs.Format(_T("%.3f"), h.dr);
	tRadialDr.SetWindowTextW(cs);
	cs = (h.udr).c_str();
	cRadialDr.SelectString(-1,cs);

	cs.Format(_T("%.2e"), h.K1);
	tRadialK1.SetWindowTextW(cs);

	cs.Format(_T("%.2e"), h.K2);
	tRadialK2.SetWindowTextW(cs);

	cs.Format(_T("%.2e"), h.L1);
	tRadialL1.SetWindowTextW(cs);

	cs.Format(_T("%.2e"), h.L2);
	tRadialL2.SetWindowTextW(cs);
	
	return TRUE;
}

BOOL CRadialInput::Save(CString lpszPathName)
{
	CRadialInput::Read();

	//save to file
	ofstream outs(lpszPathName);
	outs << h;
	outs.close();
	return TRUE;
}

void CRadialInput::Read()
{
	CString cs;

	tRadialRw.GetWindowTextW(cs);
	h.Rw = atof(CT2CA(cs));
	cRadialRw.GetWindowTextW(cs);
	h.uRw = CT2CA(cs);

	tRadialRe.GetWindowTextW(cs);
	h.Re = atof(CT2CA(cs));
	cRadialRe.GetWindowTextW(cs);
	h.uRe = CT2CA(cs);

	tRadialRinf.GetWindowTextW(cs);
	h.Rinf = atof(CT2CA(cs));
	cRadialRinf.GetWindowTextW(cs);
	h.uRinf = CT2CA(cs);

	tRadialC0.GetWindowTextW(cs);
	h.C0 = atof(CT2CA(cs));
	cRadialC0.GetWindowTextW(cs);
	h.uC0 = CT2CA(cs);

	tRadialP0.GetWindowTextW(cs);
	h.P0 = atof(CT2CA(cs));
	cRadialP0.GetWindowTextW(cs);
	h.uP0 = CT2CA(cs);

	tRadialCf.GetWindowTextW(cs);
	h.cf = atof(CT2CA(cs));
	cRadialCf.GetWindowTextW(cs);
	h.ucf = CT2CA(cs);

	tRadialNions.GetWindowTextW(cs);
	h.n = atoi(CT2CA(cs));

	tRadialMw.GetWindowTextW(cs);
	h.M = atof(CT2CA(cs));

	tRadialTemp.GetWindowTextW(cs);
	h.Temp = atof(CT2CA(cs));
	cRadialTemp.GetWindowTextW(cs);
	h.uTemp = CT2CA(cs);

	tRadialTime.GetWindowTextW(cs);
	h.t = atof(CT2CA(cs));
	cRadialTime.GetWindowTextW(cs);
	h.ut = CT2CA(cs);

	tRadialCdf.GetWindowTextW(cs);
	h.Cdf = atof(CT2CA(cs));
	cRadialCdf.GetWindowTextW(cs);
	h.uCdf = CT2CA(cs);

	tRadialPw.GetWindowTextW(cs);
	h.Pw = atof(CT2CA(cs));
	cRadialPw.GetWindowTextW(cs);
	h.uPw = CT2CA(cs);

	tRadialDt.GetWindowTextW(cs);
	h.dt = atof(CT2CA(cs));
	cRadialDt.GetWindowTextW(cs);
	h.udt = CT2CA(cs);

	tRadialDr.GetWindowTextW(cs);
	h.dr = atof(CT2CA(cs));
	cRadialDr.GetWindowTextW(cs);
	h.udr = CT2CA(cs);

	tRadialK1.GetWindowTextW(cs);
	h.K1 = atof(CT2CA(cs));

	tRadialK2.GetWindowTextW(cs);
	h.K2 = atof(CT2CA(cs));

	tRadialL1.GetWindowTextW(cs);
	h.L1 = atof(CT2CA(cs));

	tRadialL2.GetWindowTextW(cs);
	h.L2 = atof(CT2CA(cs));
}

BEGIN_MESSAGE_MAP(CRadialInput, CPropertyPage)
	ON_COMMAND(ID_FILE_READEXPERIMENTDATA, &CRadialInput::OnFileReadexperimentdata)
END_MESSAGE_MAP()


// CRadialInput message handlers

void CRadialInput::OnFileReadexperimentdata()
{
	//filter file types
	LPCTSTR pszFilter = _T("Input files (*.in)|*.in| All files (*.*)|*.*||");

	CFileDialog dlgFile(TRUE, NULL, NULL, OFN_HIDEREADONLY, pszFilter, AfxGetMainWnd());

	if(dlgFile.DoModal() == IDOK)
	{
		CString lpszPathName = dlgFile.GetPathName();
		//CWinApp::OpenDocumentFile(lpszPathName);

		//ifstream ins(lpszPathName);
		//std::string mode = ReadAString(ins);

		/*find active PropertyPage (linear or radial input)
		CChildFrame * pChild = (CChildFrame*)(((CMainFrame*)AfxGetMainWnd())->MDIGetActive());
		CTabView* pForm = (CTabView*)pChild->m_wndSplitter.GetPane(0,0);
		CTabSheet* pTab = (CTabSheet*)pForm->m_Tab;
		//CPropertyPage* pPage = pTab->GetActivePage();

		CLinearInput* pLinear = &(pTab->m_Linear);
		CRadialInput* pRadial = &(pTab->m_Radial);

		/*if (pPage)
		{
		BOOL fileOpen;
		if (pPage == pLinear) fileOpen = pLinear->Open(lpszPathName);
		if (pPage == pRadial) fileOpen = pRadial->Open(lpszPathName);
		}*/

		//BOOL fileOpen;
		//if (mode == "Linear")
		//{
		//	pTab->SetActivePage(pLinear);
			//fileOpen = pLinear->Open(lpszPathName);
		//}
		//else if (mode == "Radial")
		//{
		//	pTab->SetActivePage(pRadial);
		//	fileOpen = pRadial->Open(lpszPathName);
		//}
	}
}
