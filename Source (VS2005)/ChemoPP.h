// ChemoPP.h : main header file for the ChemoPP application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CChemoPPApp:
// See ChemoPP.cpp for the implementation of this class
//

class CChemoPPApp : public CWinApp
{
public:
	CChemoPPApp();


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnFileOpen();
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CChemoPPApp theApp;