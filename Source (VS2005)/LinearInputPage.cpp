// LinearInputPage.cpp : implementation file
//

#include "stdafx.h"
#include "ChemoPP.h"
#include "LinearInputPage.h"
#include "Unit/Unit.h"
#include "Calc/LinearPP.h"


// CLinearInputPage dialog

IMPLEMENT_DYNAMIC(CLinearInputPage, CPropertyPage)

CLinearInputPage::CLinearInputPage()
	: CPropertyPage(CLinearInputPage::IDD)
{

}

CLinearInputPage::~CLinearInputPage()
{
}

void CLinearInputPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO1, cLinearLength);
	DDX_Control(pDX, IDC_COMBO3, cLinearP0);
	DDX_Control(pDX, IDC_COMBO4, cLinearCf);
	DDX_Control(pDX, IDC_COMBO2, cLinearTemp);
	DDX_Control(pDX, IDC_COMBO9, cLinearTime);
	DDX_Control(pDX, IDC_COMBO5, cLinearCdf);
	DDX_Control(pDX, IDC_COMBO6, cLinearPdf);
	DDX_Control(pDX, IDC_COMBO7, cLinearDt);
	DDX_Control(pDX, IDC_COMBO8, cLinearDx);
	DDX_Control(pDX, IDC_COMBO11, cLinearC0);
}

BOOL CLinearInputPage::OnInitDialog()
{
	CPropertyPage::OnInitDialog();
	ComboBoxFill();

	return TRUE;
}

void CLinearInputPage::ComboBoxFill()
{
	UnitCollection* ptr = UnitCollection::Instance("Units.txt"); //initialize
	
	//populating comboboxes
	Unit* pU;
	const vector<Unit_Convertor>* pUl;
	vector<Unit_Convertor>::const_iterator i;

	//length unit type
	pU = ptr->GetUnit("Length");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		std::string s = (*i).Unit_Name;
		std::wstring ws;
		ws.assign(s.begin(),s.end()); //convert from string to wstring

		cLinearLength.AddString(ws.c_str()); //core sample length
		cLinearDx.AddString(ws.c_str()); //dx
	}
	
	//pressure unit type
	pU = ptr->GetUnit("Pressure");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		std::string s = (*i).Unit_Name;
		std::wstring ws;
		ws.assign(s.begin(),s.end()); //convert from string to wstring

		cLinearP0.AddString(ws.c_str()); //initial pore pressure
		cLinearPdf.AddString(ws.c_str()); //test fluid pressure
	}

	//concentration (mass basis) unit type
	pU = ptr->GetUnit("Concentration");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		std::string s = (*i).Unit_Name;
		std::wstring ws;
		ws.assign(s.begin(),s.end()); //convert from string to wstring

		cLinearCdf.AddString(ws.c_str()); //test fluid concentration
	}

	//concentration (mol basis) unit type
	pU = ptr->GetUnit("Concentration (mol basis)");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		std::string s = (*i).Unit_Name;
		std::wstring ws;
		ws.assign(s.begin(),s.end()); //convert from string to wstring

		cLinearC0.AddString(ws.c_str()); //initial pore fluid concentration
	}

	//compressibility unit type
	pU = ptr->GetUnit("Compressibility");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		std::string s = (*i).Unit_Name;
		std::wstring ws;
		ws.assign(s.begin(),s.end()); //convert from string to wstring

		cLinearCf.AddString(ws.c_str()); //pore fluid compressibility
	}

	//temperature unit type
	pU = ptr->GetUnit("Temperature");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		std::string s = (*i).Unit_Name;
		std::wstring ws;
		ws.assign(s.begin(),s.end()); //convert from string to wstring

		cLinearTemp.AddString(ws.c_str()); //temperature
	}

	//time unit type
	pU = ptr->GetUnit("Time");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		std::string s = (*i).Unit_Name;
		std::wstring ws;
		ws.assign(s.begin(),s.end()); //convert from string to wstring

		cLinearTime.AddString(ws.c_str()); //total time
		cLinearDt.AddString(ws.c_str()); //dt
	}
}

BOOL CLinearInputPage::Open(LPCTSTR lpszPathName)
{
	ifstream ins(lpszPathName);
	ins >> h;
	ins.close();
	return TRUE;
}

BOOL CLinearInputPage::Save(LPCTSTR lpszPathName)
{
	ofstream outs(lpszPathName);
	outs << h;
	outs.close();
	return TRUE;
}

BEGIN_MESSAGE_MAP(CLinearInputPage, CPropertyPage)
END_MESSAGE_MAP()


// CLinearInputPage message handlers
