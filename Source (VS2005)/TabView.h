#pragma once

#include "TabSheet.h"

// CTabView form view

class CTabView : public CFormView
{
	DECLARE_DYNCREATE(CTabView)

protected:
	CTabView();           // protected constructor used by dynamic creation
	virtual ~CTabView();

public:
	enum { IDD = IDD_TABVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	
	DECLARE_MESSAGE_MAP()

public:
	CTabSheet* m_Tab;
	virtual void OnInitialUpdate();
};


