#pragma once

#include "Chart/ChartCtrl.h"
#include <vector>
#include "afxwin.h"

using namespace std;


// CChartView form view

class CChartView : public CFormView
{
	DECLARE_DYNCREATE(CChartView)

protected:
	CChartView();           // protected constructor used by dynamic creation
	virtual ~CChartView();

public:
	enum { IDD = IDD_CHARTVIEW };
#ifdef _DEBUG
	virtual void AssertValid() const;
#ifndef _WIN32_WCE
	virtual void Dump(CDumpContext& dc) const;
#endif
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

public:
	CChartCtrl m_ChartCtrl;
	virtual void OnInitialUpdate();
	virtual void Plot(const vector<double>& Pdata, const vector<double>& tdata,const vector<double>& Edata,const vector<double>& ETdata);
	CEdit cNPlot;
};


