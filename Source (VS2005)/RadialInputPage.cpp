// RadialInputPage.cpp : implementation file
//

#include "stdafx.h"
#include "ChemoPP.h"
#include "RadialInputPage.h"
#include "Unit/Unit.h"


// CRadialInputPage dialog

IMPLEMENT_DYNAMIC(CRadialInputPage, CPropertyPage)

CRadialInputPage::CRadialInputPage()
	: CPropertyPage(CRadialInputPage::IDD)
{

}

CRadialInputPage::~CRadialInputPage()
{
}

void CRadialInputPage::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO1, cRadialRw);
	DDX_Control(pDX, IDC_COMBO10, cRadialRe);
	DDX_Control(pDX, IDC_COMBO3, cRadialP0);
	DDX_Control(pDX, IDC_COMBO4, cRadialCf);
	DDX_Control(pDX, IDC_COMBO2, cRadialTemp);
	DDX_Control(pDX, IDC_COMBO9, cRadialTime);
	DDX_Control(pDX, IDC_COMBO5, cRadialCdf);
	DDX_Control(pDX, IDC_COMBO6, cRadialPw);
	DDX_Control(pDX, IDC_COMBO7, cRadialDt);
	DDX_Control(pDX, IDC_COMBO8, cRadialDr);
	DDX_Control(pDX, IDC_COMBO12, cRadialC0);
}

BOOL CRadialInputPage::OnInitDialog()
{
	CPropertyPage::OnInitDialog();
	ComboBoxFill();

	return TRUE;
}

void CRadialInputPage::ComboBoxFill()
{
	UnitCollection* ptr = UnitCollection::Instance("Units.txt"); //initialize
	
	//populating comboboxes
	Unit* pU;
	const vector<Unit_Convertor>* pUl;
	vector<Unit_Convertor>::const_iterator i;

	//length unit type
	pU = ptr->GetUnit("Length");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		std::string s = (*i).Unit_Name;
		std::wstring ws;
		ws.assign(s.begin(),s.end()); //convert from string to wstring

		cRadialRw.AddString(ws.c_str()); //wellbore radius
		cRadialRe.AddString(ws.c_str()); //radius of investigation
		cRadialDr.AddString(ws.c_str()); //dr
	}
	
	//pressure unit type
	pU = ptr->GetUnit("Pressure");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		std::string s = (*i).Unit_Name;
		std::wstring ws;
		ws.assign(s.begin(),s.end()); //convert from string to wstring

		cRadialP0.AddString(ws.c_str()); //initial pore pressure
		cRadialPw.AddString(ws.c_str()); //drilling fluid pressure
	}

	//concentration (mass basis) unit type
	pU = ptr->GetUnit("Concentration");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		std::string s = (*i).Unit_Name;
		std::wstring ws;
		ws.assign(s.begin(),s.end()); //convert from string to wstring

		cRadialCdf.AddString(ws.c_str()); //drilling fluid concentration
	}

	//concentration (mol basis) unit type
	pU = ptr->GetUnit("Concentration (mol basis)");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		std::string s = (*i).Unit_Name;
		std::wstring ws;
		ws.assign(s.begin(),s.end()); //convert from string to wstring

		cRadialC0.AddString(ws.c_str()); //initial pore fluid concentration
	}

	//compressibility unit type
	pU = ptr->GetUnit("Compressibility");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		std::string s = (*i).Unit_Name;
		std::wstring ws;
		ws.assign(s.begin(),s.end()); //convert from string to wstring

		cRadialCf.AddString(ws.c_str()); //pore fluid compressibility
	}

	//temperature unit type
	pU = ptr->GetUnit("Temperature");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		std::string s = (*i).Unit_Name;
		std::wstring ws;
		ws.assign(s.begin(),s.end()); //convert from string to wstring

		cRadialTemp.AddString(ws.c_str()); //temperature
	}

	//time unit type
	pU = ptr->GetUnit("Time");
	pUl = pU->GetUnitsList();
	
	for (i=pUl->begin(); i!=pUl->end(); ++i)
	{
		std::string s = (*i).Unit_Name;
		std::wstring ws;
		ws.assign(s.begin(),s.end()); //convert from string to wstring

		cRadialTime.AddString(ws.c_str()); //total time
		cRadialDt.AddString(ws.c_str()); //dt
	}
}

BEGIN_MESSAGE_MAP(CRadialInputPage, CPropertyPage)
END_MESSAGE_MAP()


// CRadialInputPage message handlers
