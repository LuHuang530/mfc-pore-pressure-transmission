// TabSheet.cpp : implementation file
//

#include "stdafx.h"
#include "ChemoPP.h"
#include "TabSheet.h"

#include "MainFrm.h"
#include "ChildFrm.h"
#include "ChartView.h"
#include "general.h"
#include <fstream>
#include <vector>

using namespace std;

// CTabSheet

IMPLEMENT_DYNAMIC(CTabSheet, CPropertySheet)

CTabSheet::CTabSheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
	AddPage(&m_Linear);
	AddPage(&m_Radial);
}

CTabSheet::CTabSheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
	:CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
	AddPage(&m_Linear);
	AddPage(&m_Radial);
}

CTabSheet::~CTabSheet()
{
}

BOOL CTabSheet::OnInitDialog()
{
	CPropertySheet::OnInitDialog();
	
	CRect rectT, rectB;
	//move TabSheet to top left
	this->GetWindowRect(&rectT);
	this->SetWindowPos(NULL,0,0,rectT.Width()+100,rectT.Height()+50,SWP_NOZORDER);
	
	////move ok button
	//GetDlgItem(IDOK)->GetWindowRect(&rectB);
	//ScreenToClient(&rectB);
	//GetDlgItem(IDOK)->SetWindowPos(NULL,rectT.Width()-rectB.Width(),0,rectB.Width(),rectB.Height(),SWP_SHOWWINDOW);
	//GetDlgItem(IDOK)->EnableWindow(TRUE);
	
	//buttons settings
	CWnd* pButton;

	//OK button
	pButton = GetDlgItem(IDOK);
	pButton->ShowWindow(FALSE);
	pButton->EnableWindow(FALSE);
	//pButton->SetWindowText(_T("&Save"));

	//Cancel button
	pButton = GetDlgItem(IDCANCEL);
	pButton->ShowWindow(FALSE);
	pButton->EnableWindow(FALSE);
	//pButton->SetWindowText(_T("&Load"));

	//Apply button
	pButton = GetDlgItem(ID_APPLY_NOW);
	pButton->ShowWindow(TRUE);
	pButton->EnableWindow(TRUE);
	//pButton->SetWindowText(_T("&Apply"));

	//Help button
	pButton = GetDlgItem(IDHELP);
	pButton->ShowWindow(FALSE);
	pButton->EnableWindow(FALSE);
	//pButton->SetWindowText(_T("&Cancel"));
	

	return TRUE;
}

BEGIN_MESSAGE_MAP(CTabSheet, CPropertySheet)
	ON_COMMAND(ID_APPLY_NOW, &CTabSheet::OnApplyNow)
END_MESSAGE_MAP()

void CTabSheet::OnApplyNow()
{
	CPropertyPage* pPage = this->GetActivePage();

	CLinearInput* pLinear = &m_Linear;
	CRadialInput* pRadial = &m_Radial;
	
	vector<double> Pdata;
	vector<double> tdata;

	ifstream ins("experiment.txt");
	vector<double> Edata;
	vector<double> ETdata;
	while(!ins.eof())
	{
		double et;
		double ed;
		ins>>et;
		ETdata.push_back(et);
		ins>>ed;
		Edata.push_back(ed);
	}
	ins.close();
	if (pPage)
	{
		if (pPage == pLinear) pLinear->Compute(Pdata, tdata);
		if (pPage == pRadial) pRadial->Compute();
	}

	//find the chart view
	CChildFrame * pChild = (CChildFrame*)(((CMainFrame*)this->GetParentOwner())->MDIGetActive());
	CChartView* pChart = (CChartView*)pChild->m_wndSplitter.GetPane(0,1);
	pChart->Plot(Pdata, tdata,Edata,ETdata);
}

// CTabSheet message handlers
